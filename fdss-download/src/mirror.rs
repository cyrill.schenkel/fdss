use reqwest::{blocking, redirect::Policy, Method, Url};
use std::{
    fmt::Display,
    time::{Duration, Instant},
};
use thiserror::Error;

const MEASUREMENTS: usize = 1;

#[derive(Error, Debug)]
pub enum MirrorError {
    #[error("latency test failed")]
    TestFailed,
    #[error("cannot choose mirror from zero options")]
    NoChoices,
    #[error("cannot parse URL: {0}")]
    UrlFormatError(String),
    #[error("mirror choice failed: {0}")]
    Other(String),
}

impl MirrorError {
    fn from(e: impl Display) -> Self {
        Self::Other(e.to_string())
    }
}

pub struct Mirrors {
    // assumption: small number of mirrors known in advance means vec > b-tree
    mirrors: Vec<Mirror>,
    last_test: Instant,
}

impl Mirrors {
    // TODO parallelize to improve speed
    pub fn choose_best(&mut self, urls: &[String]) -> Result<Url, MirrorError> {
        if urls.is_empty() {
            return Err(MirrorError::NoChoices);
        }
        let mut best_latency = f32::MAX;
        let mut best_url: Url =
            Url::parse(&urls[0]).map_err(|_| MirrorError::UrlFormatError(urls[0].to_owned()))?;

        let retest = self.last_test.elapsed() > Duration::from_secs(60);
        if retest {
            self.last_test = Instant::now();
        }

        'outer: for url in urls {
            let url: Url =
                Url::parse(url).map_err(|_| MirrorError::UrlFormatError(url.to_owned()))?;
            for mirror in self.mirrors.iter_mut() {
                if retest {
                    mirror.retest()?;
                }
                if Some(&mirror.host[..]) == url.host_str() {
                    if mirror.latency < best_latency {
                        best_latency = mirror.latency;
                        best_url = url.to_owned();
                    }
                    continue 'outer;
                }
            }
            let mirror = Mirror::new(&url)?;
            if mirror.latency < best_latency {
                best_latency = mirror.latency;
                best_url = url;
            }
            self.mirrors.push(mirror);
        }
        Ok(best_url)
    }
}

impl Default for Mirrors {
    fn default() -> Self {
        Self {
            mirrors: Vec::new(),
            last_test: Instant::now(),
        }
    }
}

struct Mirror {
    pub latency: f32,
    pub host: String,
    test_url: Url,
}

impl Mirror {
    fn new(url: &Url) -> Result<Self, MirrorError> {
        let host = url.host().ok_or(MirrorError::TestFailed)?.to_string();
        let latency = test_latency(url)?;
        Ok(Self {
            latency,
            host,
            test_url: url.to_owned(),
        })
    }

    fn retest(&mut self) -> Result<(), MirrorError> {
        let latency = test_latency(&self.test_url)?;
        self.latency = (self.latency + latency) / 2.0;
        Ok(())
    }
}

fn test_latency(url: &Url) -> Result<f32, MirrorError> {
    let client = blocking::ClientBuilder::new()
        .tcp_keepalive(None)
        .redirect(Policy::none())
        .build()
        .map_err(MirrorError::from)?;
    let start = Instant::now();
    for _ in 0..MEASUREMENTS {
        let request = blocking::Request::new(Method::HEAD, url.to_owned());
        client.execute(request).map_err(MirrorError::from)?;
    }
    let latency = start.elapsed().as_secs_f32() / MEASUREMENTS as f32;
    Ok(latency)
}
