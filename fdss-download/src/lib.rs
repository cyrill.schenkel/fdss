mod mirror;

use std::{
    borrow::Borrow,
    cmp::min,
    ffi::OsStr,
    fmt::Display,
    fs::{create_dir_all, read_to_string},
    io::{self, Read},
    num::NonZeroU32,
    path::PathBuf,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, Condvar, Mutex,
    },
    thread::{self, JoinHandle},
    time::{Duration, Instant},
};

use anyhow::{anyhow, bail, Result};
use crossbeam::{
    channel::{unbounded, Receiver, Sender},
    queue::SegQueue,
};
use fdss_config::{Config, Docset, Feed};
use flate2::read::GzDecoder;
use gix::{
    open, prepare_clone,
    progress::Discard,
    remote::{self, fetch, Direction},
};
use log::error;
use mirror::{MirrorError, Mirrors};
use reqwest::blocking::Response;
use roxmltree::Document;
use thiserror::Error;
pub use {gix::bstr::BStr, gix::Url};

const MAX_WORKERS: usize = 4;

#[derive(Error, Debug)]
pub enum DownloadError {
    #[error("download failed: {0}")]
    Http(String),
    #[error("feed has no url")]
    MissingUrl(Feed),
    #[error("download failed: {0}")]
    Other(String),
}

impl DownloadError {
    fn from(e: impl Display) -> Self {
        Self::Other(e.to_string())
    }
}

#[derive(Eq, PartialEq, PartialOrd, Ord, Clone, Debug)]
pub struct DownloadStatus {
    pub feed: Option<Feed>,
    pub done: bool,
    pub rate: u64,
    pub size: u64,
    pub transferred: u64,
}

impl Default for DownloadStatus {
    fn default() -> Self {
        Self {
            feed: None,
            done: true,
            rate: 0,
            size: 0,
            transferred: 0,
        }
    }
}

pub struct DownloadManager {
    shutdown: Arc<AtomicBool>,
    downloads: Vec<Arc<Mutex<DownloadStatus>>>,
    signal: Arc<(Mutex<()>, Condvar)>,
    queue: Arc<SegQueue<Feed>>,
    workers: Vec<JoinHandle<()>>,
    pub finished: Receiver<Result<Docset, DownloadError>>,
}

impl DownloadManager {
    pub fn new(config: Config) -> Self {
        let count = min(num_cpus::get(), MAX_WORKERS);
        let shutdown = Arc::new(AtomicBool::new(false));
        let mut downloads = Vec::new();
        let signal: Arc<(Mutex<()>, Condvar)> = Arc::default();
        let queue: Arc<SegQueue<Feed>> = Arc::default();
        let mut workers = Vec::new();
        let (sender, receiver) = unbounded();
        let mirrors: Arc<Mutex<Mirrors>> = Arc::default();
        for _ in 0..count {
            let status: Arc<Mutex<DownloadStatus>> = Arc::default();
            downloads.push(status.clone());
            let worker = DownloadWorker::new(
                config.clone(),
                shutdown.clone(),
                signal.clone(),
                status.clone(),
                queue.clone(),
                mirrors.clone(),
                sender.clone(),
            );
            workers.push(thread::spawn(move || worker.run()));
        }
        Self {
            shutdown,
            downloads,
            signal,
            queue,
            workers,
            finished: receiver,
        }
    }

    pub fn idle(&self) -> bool {
        self.queue_depth() == 0
            && !self
                .downloads
                .iter()
                .any(|status| !status.lock().expect("mutex poisoned").done)
    }

    pub fn queue_depth(&self) -> usize {
        self.queue.len()
    }

    pub fn downloads(&self) -> Vec<DownloadStatus> {
        let mut result = Vec::new();
        for status in self.downloads.iter() {
            let status = status.lock().expect("mutex poisoned").clone();
            if !status.done && status.feed.is_some() {
                result.push(status);
            }
        }
        result
    }

    pub fn download(&self, feed: Feed) -> Result<()> {
        let (sig_mutex, sig_condvar) = self.signal.borrow();
        if let Err(e) = sig_mutex.lock() {
            bail!("failed to lock mutex: {:?}", e);
        }
        self.queue.push(feed);
        sig_condvar.notify_one();
        Ok(())
    }

    pub fn shutdown(self) -> Result<()> {
        let (sig_mutex, sig_condvar) = self.signal.borrow();
        if let Err(e) = sig_mutex.lock() {
            bail!("failed to lock mutex: {:?}", e);
        }
        self.shutdown.store(true, Ordering::Relaxed);
        sig_condvar.notify_all();
        for worker in self.workers {
            worker
                .join()
                .map_err(|e| anyhow!("failed during wait on thread: {:?}", e))?;
        }
        Ok(())
    }
}

struct DownloadWorker {
    config: Config,
    shutdown: Arc<AtomicBool>,
    signal: Arc<(Mutex<()>, Condvar)>,
    status: Arc<Mutex<DownloadStatus>>,
    queue: Arc<SegQueue<Feed>>,
    mirrors: Arc<Mutex<Mirrors>>,
    finished: Sender<Result<Docset, DownloadError>>,
}

impl DownloadWorker {
    fn new(
        config: Config,
        shutdown: Arc<AtomicBool>,
        signal: Arc<(Mutex<()>, Condvar)>,
        status: Arc<Mutex<DownloadStatus>>,
        queue: Arc<SegQueue<Feed>>,
        mirrors: Arc<Mutex<Mirrors>>,
        finished: Sender<Result<Docset, DownloadError>>,
    ) -> Self {
        Self {
            config,
            shutdown,
            signal,
            status,
            queue,
            mirrors,
            finished,
        }
    }

    fn run(&self) {
        while !self.shutdown.load(Ordering::Relaxed) {
            if let Some(feed) = self.queue.pop() {
                if let Err(e) = self
                    .finished
                    .send(self.download(feed, &self.config, &self.status))
                {
                    error!("failed to report succesful download: {}", e);
                }
            } else {
                let (sig_mutex, sig_condvar) = self.signal.borrow();
                drop(
                    sig_condvar
                        .wait(sig_mutex.lock().expect("mutex is poisoned"))
                        .expect("mutex is poisoned"),
                );
            }
        }
    }

    fn download(
        &self,
        feed: Feed,
        config: &Config,
        status: &Mutex<DownloadStatus>,
    ) -> Result<Docset, DownloadError> {
        match status.lock() {
            Ok(mut status) => {
                status.feed = Some(feed.clone());
                status.done = false;
                status.rate = 0;
                status.transferred = 0;
            }
            Err(e) => return Err(DownloadError::from(e)),
        }
        let url = self
            .mirrors
            .lock()
            .map_err(DownloadError::from)
            .and_then(|mut mirrors| {
                mirrors.choose_best(&feed.urls).map_err(|e| match e {
                    MirrorError::NoChoices => DownloadError::MissingUrl(feed.clone()),
                    e => DownloadError::from(e),
                })
            })?;
        let response = reqwest::blocking::get(url).map_err(DownloadError::from)?;
        let status_code = response.status();
        if status_code.is_server_error() || status_code.is_client_error() {
            if let Ok(mut status) = status.lock() {
                status.feed = None;
                status.done = true;
                status.size = 0;
            }
            return Err(DownloadError::Http(status_code.to_string()));
        }
        match status.lock() {
            Ok(mut status) => {
                status.size = response.content_length().unwrap_or(0);
            }
            Err(e) => return Err(DownloadError::from(e)),
        }
        let recorder = DownloadStatusRecorder::new(status, response);
        let tar = GzDecoder::new(recorder);
        let mut archive = tar::Archive::new(tar);
        archive
            .unpack(feed.install_path(config))
            .map_err(DownloadError::from)?;
        match status.lock() {
            Ok(mut status) => {
                status.done = true;
                status.rate = 0;
                status.transferred = status.size;
            }
            Err(e) => return Err(DownloadError::from(e)),
        }
        Ok(Docset::from_feed(feed, config))
    }
}

struct DownloadStatusRecorder<'a> {
    status: &'a Mutex<DownloadStatus>,
    inner: Response,
    start: Instant,
    transferred: u64,
}

impl<'a> DownloadStatusRecorder<'a> {
    fn new(status: &'a Mutex<DownloadStatus>, inner: Response) -> Self {
        Self {
            status,
            inner,
            start: Instant::now(),
            transferred: 0,
        }
    }
}

impl<'a> Read for DownloadStatusRecorder<'a> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        let bytes_read = self.inner.read(buf)?;
        self.transferred += bytes_read as u64;
        let delta = self.start.elapsed();
        if delta > Duration::from_millis(300) {
            match self.status.lock() {
                Ok(mut status) => {
                    let new_rate = self.transferred as f32 / delta.as_secs_f32();
                    let filtered_rate = (new_rate + status.rate as f32) / 2.0;
                    status.rate = filtered_rate as u64;
                    status.transferred += self.transferred;
                    self.transferred = 0;
                    self.start = Instant::now();
                }
                Err(e) => {
                    return Err(io::Error::new(
                        io::ErrorKind::Other,
                        anyhow!("mutex poisoned: {:?}", e),
                    ))
                }
            }
        }
        Ok(bytes_read)
    }
}

pub fn parse_feed(path: PathBuf) -> Result<Feed> {
    let name = path
        .file_stem()
        .ok_or(anyhow!("malformed path: {:?}", path))?
        .to_string_lossy()
        .replace('_', " ");
    let mut urls = Vec::new();
    let mut version = String::new();
    let xml_content = read_to_string(path)?;
    let xml = Document::parse(&xml_content)?;
    for descendant in xml.descendants() {
        if descendant.has_tag_name("version") {
            if let Some(text) = descendant.text() {
                version.push_str(text);
            }
        }
        if descendant.has_tag_name("url") {
            if let Some(text) = descendant.text() {
                urls.push(text.to_string());
            }
        }
    }
    Ok(Feed::new(name, version, urls))
}

pub fn read_git_feeds(url: &Url) -> Result<impl Iterator<Item = Result<Feed>>> {
    let dir = git_feeds_dir(url)?;
    let reader = dir.read_dir()?;
    Ok(reader.filter_map(|entry| match entry {
        Ok(entry) => {
            let path = entry.path();
            if path.extension() == Some(OsStr::new("xml")) {
                Some(parse_feed(path))
            } else {
                None
            }
        }
        Err(e) => Some(Err(anyhow!("cannot read directory: {}", e))),
    }))
}

pub fn fetch_git_feeds(url: &Url) -> Result<()> {
    let dir = git_feeds_dir(url)?;
    let interrupt = AtomicBool::new(false);
    if dir.exists() {
        let repository = open(dir)?;
        let remote = repository
            .find_default_remote(Direction::Fetch)
            .ok_or(anyhow!("default remote not found"))??;
        let conn = remote.connect(Direction::Fetch)?;
        conn.prepare_fetch(Discard, remote::ref_map::Options::default())?
            .with_shallow(fetch::Shallow::DepthAtRemote(unsafe {
                NonZeroU32::new_unchecked(1)
            }))
            .receive(Discard, &interrupt)?;
    } else {
        let parent_dir = dir.parent().ok_or(anyhow!("illegal cache dir"))?;
        if !parent_dir.exists() {
            create_dir_all(parent_dir)?;
        }
        let pre_fetch = prepare_clone(url.clone(), dir)?;
        let (mut pre_checkout, _outcome) = pre_fetch
            .with_shallow(fetch::Shallow::DepthAtRemote(unsafe {
                NonZeroU32::new_unchecked(1)
            }))
            .fetch_then_checkout(Discard, &interrupt)?;
        pre_checkout.main_worktree(Discard, &interrupt)?;
    }
    Ok(())
}

fn git_feeds_dir(url: &Url) -> Result<PathBuf> {
    let mut path = dirs::cache_dir().ok_or(anyhow!("unable to determine user cache directory"))?;
    path.push("fdss");
    let mut dir_name = url.scheme.to_string();
    dir_name.push('.');
    if let Some(host) = url.host() {
        dir_name.push_str(&host.replace('.', "-"));
        dir_name.push('.')
    }
    dir_name.push_str(&url.path.to_string().replace('/', "_").replace('.', "-"));
    path.push(dir_name);
    Ok(path)
}
