use crate::{DocumentWidget, RenderContext};
use log::debug;
use ratatui::{
    prelude::{Buffer, Rect},
    style::{Style, Stylize},
    text::{Line, Span},
    widgets::Widget,
};
use std::{mem, slice, str::Chars};

#[derive(Clone, Eq, PartialEq, Debug)]
pub enum Inline {
    Plain(String),
    Bold(String),
    Italic(String),
    Code(String),
    NewLine,
}

impl DocumentWidget for [Inline] {
    fn render(&self, area: Rect, buf: &mut Buffer, context: &mut RenderContext<'_>) {
        let mut rows = area.rows();
        let mut row = None;
        let mut words = StyledWords::from(self).with_base_style(context.state.style);
        let mut word = words.next();
        let mut line = Vec::new();
        let mut rem = 0;
        let mut offset = context.state.offset + 1;
        loop {
            if rem == 0 {
                if let Some(row) = row {
                    if !line.is_empty() {
                        Line::from(mem::take(&mut line)).render(row, buf);
                        context.measure_context.advance(1);
                    }
                }
                if offset > 0 {
                    offset -= 1;
                    line.clear();
                }
                if offset == 0 {
                    row = rows.next();
                }
                if row.is_none() && offset == 0 {
                    break;
                }
                rem = area.width;
            }
            if let Some(current_word) = &word {
                let word_len = current_word.len();
                if word_len > rem as usize && !current_word.starts_with_newline() {
                    if word_len > area.width as usize {
                        // TOOD split long words
                        debug!("swallow word: {:?}", current_word);
                        word = words.next();
                    }
                    rem = 0;
                } else {
                    let StyledWord(spans) = current_word;
                    for (text, style) in spans {
                        if line.is_empty() {
                            let text = text.trim_start();
                            rem -= text.len() as u16;
                            line.push(Span::styled(text, *style));
                        } else {
                            rem -= text.len() as u16;
                            line.push(Span::styled(*text, *style));
                        }
                    }
                    word = words.next();
                }
                if let Some(next_word) = &word {
                    if next_word.starts_with_newline() {
                        rem = 0;
                        let StyledWord(spans) = next_word;
                        word = Some(StyledWord(spans[1..].to_vec()));
                    }
                }
            } else {
                rem = 0;
            }
        }
    }

    fn measure(&self, width: u16, context: &mut RenderContext<'_>) -> usize {
        let mut height = 0;
        let mut rem = 0;
        let mut prev_boundary = 0;

        for c in InlineChars::from(self) {
            if rem == 0 || c == '\n' {
                if prev_boundary < width - 1 && c != ' ' {
                    rem = width - prev_boundary;
                    prev_boundary = 0;
                } else {
                    rem = width;
                }
                height += 1;
            }
            if word_boundary(c) {
                prev_boundary = 0;
            } else {
                prev_boundary += 1;
            }
            if rem != width || c != ' ' {
                rem -= 1;
            }
        }
        context.measure_context.advance(height);

        height
    }
}

struct InlineChars<'a, It>
where
    It: Iterator<Item = &'a Inline>,
{
    items: It,
    chars: Option<Chars<'a>>,
}

impl<'a> From<&'a [Inline]> for InlineChars<'a, slice::Iter<'a, Inline>> {
    fn from(value: &'a [Inline]) -> Self {
        Self {
            items: value.iter(),
            chars: None,
        }
    }
}

impl<'a, It> Iterator for InlineChars<'a, It>
where
    It: Iterator<Item = &'a Inline>,
{
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(chars) = &mut self.chars {
            if let Some(char) = chars.next() {
                return Some(char);
            }
        }
        if let Some(item) = self.items.next() {
            self.chars = match item {
                Inline::Plain(text) => Some(text.chars()),
                Inline::Bold(text) => Some(text.chars()),
                Inline::Italic(text) => Some(text.chars()),
                Inline::Code(text) => Some(text.chars()),
                Inline::NewLine => return Some('\n'),
            };
            self.next()
        } else {
            None
        }
    }
}

#[derive(Debug)]
struct StyledWord<'a>(Vec<(&'a str, Style)>);

impl<'a> StyledWord<'a> {
    fn len(&self) -> usize {
        let Self(spans) = self;
        let mut size = 0;
        for (text, _) in spans.iter() {
            size += text.len();
        }
        size
    }

    fn starts_with_newline(&self) -> bool {
        let Self(spans) = self;
        !spans.is_empty() && spans[0].0 == "\n"
    }
}

struct StyledWords<'a, It>
where
    It: Iterator<Item = &'a Inline>,
{
    items: It,
    text: Option<&'a str>,
    base_style: Style,
    style: Style,
    incomplete: Vec<(&'a str, Style)>,
}

impl<'a, It> StyledWords<'a, It>
where
    It: Iterator<Item = &'a Inline>,
{
    fn with_base_style(mut self, style: Style) -> Self {
        self.style = style;
        self.base_style = style;
        self
    }
}

impl<'a> From<&'a [Inline]> for StyledWords<'a, slice::Iter<'a, Inline>> {
    fn from(value: &'a [Inline]) -> Self {
        Self {
            items: value.iter(),
            text: None,
            base_style: Style::default(),
            style: Style::default(),
            incomplete: Default::default(),
        }
    }
}

impl<'a, It> Iterator for StyledWords<'a, It>
where
    It: Iterator<Item = &'a Inline>,
{
    type Item = StyledWord<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(text) = self.text {
            if text.starts_with(word_boundary) && !self.incomplete.is_empty() {
                return Some(StyledWord(mem::take(&mut self.incomplete)));
            }
            if let Some((word, remainder)) = text.split_word() {
                self.text = remainder;
                if remainder.is_some() && self.incomplete.is_empty() {
                    return Some(StyledWord(vec![(word, self.style)]));
                }
                self.incomplete.push((word, self.style));
                return self.next();
            } else {
                self.text = None;
            }
        }
        if let Some(item) = self.items.next() {
            self.text = match item {
                Inline::Plain(text) => {
                    self.style = self.base_style;
                    Some(text)
                }
                Inline::Bold(text) => {
                    self.style = self.base_style.bold();
                    Some(text)
                }
                Inline::Italic(text) => {
                    self.style = self.base_style.italic();
                    Some(text)
                }
                Inline::Code(text) => {
                    self.style = self.base_style.white().on_dark_gray();
                    Some(text)
                }
                Inline::NewLine => Some("\n"),
            };
            self.next()
        } else if !self.incomplete.is_empty() {
            Some(StyledWord(mem::take(&mut self.incomplete)))
        } else {
            None
        }
    }
}

pub trait StrExt {
    fn dedup_whitespace(&self) -> String;

    fn split_word(&self) -> Option<(&str, Option<&str>)>;
}

impl StrExt for str {
    fn dedup_whitespace(&self) -> String {
        let mut out = String::with_capacity(self.len());
        let mut previous = '\0';
        for char in self.chars() {
            if char.is_whitespace() {
                if !previous.is_whitespace() {
                    out.push(' ');
                }
            } else {
                out.push(char);
            }
            previous = char;
        }
        out
    }

    fn split_word(&self) -> Option<(&str, Option<&str>)> {
        if self.is_empty() {
            return None;
        }
        let mut end_of_first = 1;
        while !self.is_char_boundary(end_of_first) {
            end_of_first += 1;
        }
        if end_of_first >= self.len() {
            Some((self, None))
        } else if let Some(end) = self[end_of_first..].find(word_boundary) {
            Some((
                &self[0..(end + end_of_first)],
                Some(&self[(end + end_of_first)..]),
            ))
        } else {
            Some((self, None))
        }
    }
}

fn word_boundary(c: char) -> bool {
    !c.is_alphanumeric() && !c.is_ascii_punctuation()
}

#[cfg(test)]
mod test {
    use crate::{
        inline::{Inline, StrExt},
        DocumentState, DocumentWidget, MeasureContext, RenderContext,
    };

    #[test]
    fn plain_inline_width() {
        let context = &mut RenderContext {
            measure_context: &mut MeasureContext::default(),
            state: &mut DocumentState::default(),
        };
        let items = vec![Inline::Plain("Lorem ipsum dolor sit amet".to_string())];
        assert_eq!(items[..].measure(25, context), 2);
    }

    #[test]
    fn mixed_inline_width() {
        let context = &mut RenderContext {
            measure_context: &mut MeasureContext::default(),
            state: &mut DocumentState::default(),
        };
        let items = vec![
            Inline::Plain("1234 6789 12 456789".to_string()),
            Inline::Bold("012 45678 ".to_string()),
            Inline::Italic("01234567890123456".to_string()),
        ];
        assert_eq!(items[..].measure(7, context), 8);
    }

    #[test]
    fn split_word_one() {
        assert_eq!("abc".split_word(), Some(("abc", None)))
    }
}
