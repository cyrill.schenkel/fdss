use log::warn;

#[derive(Debug, Clone)]
pub struct SearchState {
    _query: String,
    results: Vec<(usize, u16)>,
    selected: Option<usize>,
}

impl SearchState {
    pub fn new(query: String) -> Self {
        Self {
            _query: query,
            results: vec![],
            selected: None,
        }
    }

    pub fn ensure_selected(&mut self, offset: usize) {
        if self.selected.is_none() {
            self.select_first_visible_result(offset);
        }
    }

    pub fn forward(&mut self, offset: usize) {
        if let Some(selected) = self.selected {
            self.selected = Some(selected.saturating_add(1) % self.results.len());
        } else {
            self.select_first_visible_result(offset);
        }
    }

    pub fn backward(&mut self, offset: usize) {
        if let Some(selected) = self.selected {
            self.selected = Some(selected.saturating_add(1) % self.results.len());
        } else {
            self.select_first_visible_result(offset);
        }
    }

    fn select_first_visible_result(&mut self, offset: usize) {
        self.selected = self
            .results
            .iter()
            .enumerate()
            .skip_while(|(_, (row, _))| row < &offset)
            .map(|(i, _)| i)
            .next();
        if self.selected.is_none() {
            self.selected = self.results.first().map(|_| 0);
            if self.selected.is_none() {
                warn!("no results");
            }
        }
    }
}
