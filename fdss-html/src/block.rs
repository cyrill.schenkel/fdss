use crate::{inline::Inline, DocumentWidget, RenderContext};
use ratatui::{
    prelude::{Buffer, Rect},
    style::{Style, Stylize},
    text::Line,
    widgets::{self, Widget},
};
use std::{cmp::min, ops::Range};

impl DocumentWidget for [String] {
    fn render(&self, area: Rect, buf: &mut Buffer, context: &mut RenderContext<'_>) {
        let height = min(area.height, (self.len() - context.state.offset) as u16);
        context.measure_context.advance(height as usize);
        widgets::Block::new()
            .white()
            .on_dark_gray()
            .render(Rect::new(area.x, area.y, area.width, height), buf);
        let mut rows = area.rows();
        for line in &self[context.state.offset..] {
            if let Some(row) = rows.next() {
                Line::from(&line[..]).render(row, buf);
            } else {
                break;
            }
        }
    }

    fn measure(&self, _width: u16, context: &mut RenderContext<'_>) -> usize {
        let height = self.len();
        context.measure_context.advance(height);
        height
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum HeadingLevel {
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum List {
    Unordered(Vec<ListItem>),
    Ordered(Vec<ListItem>),
    Definition(Vec<DefinitionListItem>),
}

impl List {
    fn items(&self) -> Range<usize> {
        match self {
            List::Unordered(items) => 0..items.len(),
            List::Ordered(items) => 0..items.len(),
            List::Definition(items) => 0..items.len(),
        }
    }

    fn item_indent(&self) -> u16 {
        match self {
            List::Unordered(_) => 3,
            List::Ordered(items) => items.len().checked_ilog10().unwrap_or(0) as u16 + 3,
            List::Definition(_) => 0,
        }
    }

    fn item(&self, index: usize) -> &dyn DocumentWidget {
        match self {
            List::Unordered(items) => &items[index],
            List::Ordered(items) => &items[index],
            List::Definition(items) => &items[index],
        }
    }

    fn item_height(&self, index: usize, context: &RenderContext) -> usize {
        let blocks = match self {
            List::Unordered(items) => &items[index].0,
            List::Ordered(items) => &items[index].0,
            List::Definition(items) => items[index].blocks(),
        };
        blocks
            .iter()
            .map(|block| context.section_height(block.id()))
            .sum::<usize>()
            + if blocks
                .last()
                .map(|block| context.ends_on_empty(block.id()))
                .unwrap_or(false)
            {
                0
            } else {
                1
            }
    }

    fn symbol(&self, index: usize, indent: u16) -> Line {
        match self {
            List::Unordered(_) => Line::from(" •"),
            List::Ordered(_) => Line::from(format!(
                "{:>indent$}.",
                index + 1,
                indent = indent as usize - 2
            )),
            List::Definition(_) => Line::default(),
        }
    }
}

impl DocumentWidget for List {
    fn render(&self, area: Rect, buf: &mut Buffer, context: &mut RenderContext<'_>) {
        let mut offset = context.state.offset;
        let mut spacer = context.measure_context.ensure_empty();
        if spacer <= offset {
            offset -= spacer;
            spacer = 0;
        }
        let mut rem = Rect::new(
            area.x,
            area.y + spacer as u16,
            area.width,
            area.height.saturating_sub(spacer as u16),
        );
        let indent = self.item_indent();
        let item_width = area.width.saturating_sub(indent);
        for index in self.items() {
            if rem.height == 0 {
                break;
            }
            let item = self.item(index);
            let mut height = self.item_height(index, context);
            if offset >= height {
                offset -= height;
                context.measure_context.ensure_empty();
                continue;
            }
            height -= offset;
            let (area, new_rem) = split_area(rem, height);
            rem = new_rem;
            if offset == 0 {
                self.symbol(index, indent).render(area, buf);
            }
            let item_area = Rect::new(area.x + indent, area.y, item_width, area.height);
            item.render(
                item_area,
                buf,
                &mut context.with_state(&mut context.state.with_offset(offset)),
            );
            if offset > 0 {
                offset = 0;
            }
        }
    }

    fn measure(&self, width: u16, context: &mut RenderContext<'_>) -> usize {
        let item_width = width - self.item_indent();
        context.measure_context.ensure_empty()
            + self
                .items()
                .map(|i| self.item(i).measure(item_width, context))
                .sum::<usize>()
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ListItem(pub Vec<Block>);

impl DocumentWidget for ListItem {
    fn render(&self, area: Rect, buf: &mut Buffer, context: &mut RenderContext<'_>) {
        let ListItem(blocks) = self;
        blocks[..].render(area, buf, context);
        context.measure_context.ensure_empty();
    }

    fn measure(&self, width: u16, context: &mut RenderContext<'_>) -> usize {
        let ListItem(blocks) = self;
        blocks.measure(width, context) + context.measure_context.ensure_empty()
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum DefinitionListItem {
    Term(Vec<Block>),
    Details(Vec<Block>),
}

impl DefinitionListItem {
    fn blocks(&self) -> &[Block] {
        match self {
            DefinitionListItem::Term(blocks) => blocks,
            DefinitionListItem::Details(blocks) => blocks,
        }
    }

    fn indent(&self) -> u16 {
        match self {
            DefinitionListItem::Term(_) => 0,
            DefinitionListItem::Details(_) => 4,
        }
    }

    fn customize(&self, style: Style) -> Style {
        match self {
            DefinitionListItem::Term(_) => style.bold(),
            DefinitionListItem::Details(_) => style,
        }
    }
}

impl DocumentWidget for DefinitionListItem {
    fn render(&self, area: Rect, buf: &mut Buffer, context: &mut RenderContext) {
        let indent = self.indent();
        let area = Rect::new(
            area.x + indent,
            area.y,
            area.width.saturating_sub(indent),
            area.height,
        );
        self.blocks().render(
            area,
            buf,
            &mut context.with_state(
                &mut context
                    .state
                    .with_style(self.customize(context.state.style)),
            ),
        );
        context.measure_context.ensure_empty();
    }

    fn measure(&self, width: u16, context: &mut RenderContext<'_>) -> usize {
        let width = width.saturating_sub(self.indent());
        self.blocks().measure(width, context) + context.measure_context.ensure_empty()
    }
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub enum Block {
    Heading {
        id: u32,
        level: HeadingLevel,
        text: Vec<Inline>,
    },
    Paragraph {
        id: u32,
        text: Vec<Inline>,
    },
    Code {
        id: u32,
        lines: Vec<String>,
    },
    List {
        id: u32,
        list: List,
    },
}

impl Block {
    fn id(&self) -> usize {
        match self {
            Block::Heading { id, .. } => *id as usize,
            Block::Paragraph { id, .. } => *id as usize,
            Block::Code { id, .. } => *id as usize,
            Block::List { id, .. } => *id as usize,
        }
    }
}

impl DocumentWidget for Block {
    fn render(&self, area: Rect, buf: &mut Buffer, context: &mut RenderContext<'_>) {
        match self {
            Block::Heading { level, text, .. } => render_heading(level, text, area, buf, context),
            Block::Paragraph { text, .. } => {
                text[..].render(area, buf, context);
                context.measure_context.ensure_empty();
            }
            Block::Code { lines, .. } => {
                lines.render(area, buf, context);
                context.measure_context.ensure_empty();
            }
            Block::List { list, .. } => list.render(area, buf, context),
        }
    }

    fn measure(&self, width: u16, context: &mut RenderContext<'_>) -> usize {
        if context.measure_context.marks.is_some() {
            let offset = context.measure_context.offset;
            if let Some(names) = context.measure_context.attrs.names.get(self.id()) {
                for name in names.iter() {
                    if let Some(marks) = &mut context.measure_context.marks {
                        marks.insert(name.to_owned(), offset);
                    }
                }
            }
        }
        match self {
            Block::Heading { text, .. } => text[..].measure(width, context),
            Block::Paragraph { text, .. } => {
                text[..].measure(width, context) + context.measure_context.ensure_empty()
            }
            Block::Code { lines, .. } => {
                lines.measure(width, context) + context.measure_context.ensure_empty()
            }
            Block::List { list, .. } => list.measure(width, context),
        }
    }
}

impl DocumentWidget for [Block] {
    fn render(&self, area: Rect, buf: &mut Buffer, context: &mut RenderContext<'_>) {
        let mut rem = area;
        let mut offset = context.state.offset;
        for block in self.iter() {
            if rem.height == 0 {
                break;
            }
            let id = block.id();
            let mut height = context.section_height(id);
            if offset >= height {
                offset -= height;
                context.restore_measure_context(id);
                continue;
            }
            if offset > 0 {
                height -= offset;
            }
            let (block_area, new_rem) = split_area(rem, height);
            rem = new_rem;
            block.render(
                block_area,
                buf,
                &mut context.with_state(&mut context.state.with_offset(offset)),
            );
            if offset > 0 {
                offset = 0;
            }
        }
    }

    fn measure(&self, width: u16, context: &mut RenderContext<'_>) -> usize {
        self.iter()
            .map(|block| {
                let height = block.measure(width, context);
                context.update_section_cache(block.id(), height);
                height
            })
            .sum()
    }
}

fn render_heading(
    level: &HeadingLevel,
    text: &[Inline],
    area: Rect,
    buf: &mut Buffer,
    context: &mut RenderContext,
) {
    let style = context.state.style.bold();
    let style = match level {
        HeadingLevel::H1 => style.underlined(),
        HeadingLevel::H2 => style.blue(),
        HeadingLevel::H3 => style.magenta(),
        HeadingLevel::H4 => style.yellow(),
        HeadingLevel::H5 => style.light_yellow(),
        HeadingLevel::H6 => style.light_cyan(),
    };
    let mut state = context.state.with_style(style);
    text.render(area, buf, &mut context.with_state(&mut state));
}

fn split_area(area: Rect, height: usize) -> (Rect, Rect) {
    if height >= area.height as usize {
        (
            Rect::new(
                area.x,
                area.y,
                area.width,
                min(height, area.height as usize) as u16,
            ),
            Rect::new(0, 0, 0, 0),
        )
    } else {
        (
            Rect::new(area.x, area.y, area.width, height as u16),
            Rect::new(
                area.x,
                area.y + height as u16,
                area.width,
                area.height - height as u16,
            ),
        )
    }
}

#[cfg(test)]
mod test {
    use ratatui::{buffer::Buffer, layout::Rect};

    use crate::{
        inline::Inline, Document, DocumentState, DocumentWidget, MeasureContext, RenderContext,
    };

    use super::{Block, HeadingLevel, List, ListItem};

    #[test]
    fn heading_measure() {
        let heading = Block::Heading {
            id: 0,
            level: super::HeadingLevel::H1,
            text: vec![Inline::Plain("Test Heading".to_string())],
        };
        render_measure_eq(heading, 1);
    }

    #[test]
    fn short_paragraph_measure() {
        let paragraph = Block::Paragraph {
            id: 0,
            text: vec![Inline::Plain("Test text".to_string())],
        };
        render_measure_eq(paragraph, 2);
    }

    #[test]
    fn long_paragraph_measure() {
        let paragraph = Block::Paragraph{id:0, text:vec![Inline::Plain("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed velit sit amet ex viverra vulputate non varius eros. Nunc facilisis convallis dui vel fringilla.".to_string())]};
        render_measure_eq(paragraph, 4);
    }

    #[test]
    fn code_measure() {
        let code = Block::Code {
            id: 0,
            lines: vec!["abc".to_string(), "123".to_string()],
        };
        render_measure_eq(code, 3);
    }

    #[test]
    fn list_measure() {
        let list = Block::List {
            id: 0,
            list: List::Unordered(vec![
                ListItem(vec![Block::Paragraph {
                    id: 1,
                    text: vec![Inline::Plain("abc".to_string())],
                }]),
                ListItem(vec![Block::Paragraph {
                    id: 2,
                    text: vec![Inline::Plain("123".to_string())],
                }]),
            ]),
        };
        render_measure_eq(list, 5);
    }

    #[test]
    fn heading_list_measure() {
        let doc = Document {
            attrs: Default::default(),
            blocks: vec![
                Block::Heading {
                    id: 0,
                    level: HeadingLevel::H2,
                    text: vec![Inline::Plain("abc".to_string())],
                },
                Block::List {
                    id: 1,
                    list: List::Unordered(vec![
                        ListItem(vec![Block::Paragraph {
                            id: 2,
                            text: vec![Inline::Plain("abc".to_string())],
                        }]),
                        ListItem(vec![Block::Paragraph {
                            id: 3,
                            text: vec![Inline::Plain("123".to_string())],
                        }]),
                    ]),
                },
            ],
        };
        render_measure_eq(doc, 6);
    }

    #[test]
    fn paragraph_list_measure() {
        let doc = Document {
            attrs: Default::default(),
            blocks: vec![
                Block::Paragraph {
                    id: 0,
                    text: vec![Inline::Plain("abc".to_string())],
                },
                Block::List {
                    id: 1,
                    list: List::Unordered(vec![
                        ListItem(vec![Block::Paragraph {
                            id: 2,
                            text: vec![Inline::Plain("abc".to_string())],
                        }]),
                        ListItem(vec![Block::Paragraph {
                            id: 3,
                            text: vec![Inline::Plain("123".to_string())],
                        }]),
                    ]),
                },
            ],
        };
        render_measure_eq(doc, 6);
    }

    #[test]
    fn paragraph_list_deep_measure() {
        let doc = Document {
            attrs: Default::default(),
            blocks: vec![
                Block::Paragraph {
                    id: 0,
                    text: vec![Inline::Plain("abc".to_string())],
                },
                Block::List {
                    id: 1,
                    list: List::Unordered(vec![ListItem(vec![
                        Block::Paragraph {
                            id: 2,
                            text: vec![Inline::Plain("abc".to_string())],
                        },
                        Block::List {
                            id: 3,
                            list: List::Unordered(vec![ListItem(vec![Block::Paragraph {
                                id: 4,
                                text: vec![Inline::Plain("123".to_string())],
                            }])]),
                        },
                    ])]),
                },
            ],
        };
        render_measure_eq(doc, 6);
    }

    #[test]
    fn nested_list_measure() {
        let doc = Document {
            attrs: Default::default(),
            blocks: vec![
                Block::Heading {
                    id: 0,
                    level: HeadingLevel::H2,
                    text: vec![Inline::Plain("abc".to_string())],
                },
                Block::List {
                    id: 1,
                    list: List::Unordered(vec![
                        ListItem(vec![
                            Block::Heading {
                                id: 2,
                                level: HeadingLevel::H2,
                                text: vec![Inline::Plain("abc".to_string())],
                            },
                            Block::List {
                                id: 3,
                                list: List::Unordered(vec![
                                    ListItem(vec![Block::Paragraph {
                                        id: 4,
                                        text: vec![Inline::Plain("abc".to_string())],
                                    }]),
                                    ListItem(vec![Block::Paragraph {
                                        id: 5,
                                        text: vec![Inline::Plain("123".to_string())],
                                    }]),
                                ]),
                            },
                        ]),
                        ListItem(vec![
                            Block::Heading {
                                id: 6,
                                level: HeadingLevel::H2,
                                text: vec![Inline::Plain("abc".to_string())],
                            },
                            Block::List {
                                id: 7,
                                list: List::Unordered(vec![
                                    ListItem(vec![Block::Paragraph {
                                        id: 8,
                                        text: vec![Inline::Plain("abc".to_string())],
                                    }]),
                                    ListItem(vec![Block::Paragraph {
                                        id: 9,
                                        text: vec![Inline::Plain("123".to_string())],
                                    }]),
                                ]),
                            },
                        ]),
                        ListItem(vec![Block::Paragraph {
                            id: 10,
                            text: vec![Inline::Plain("123".to_string())],
                        }]),
                    ]),
                },
            ],
        };
        render_measure_eq(doc, 16);
    }

    #[test]
    fn deep_nested_list_measure() {
        let doc = Document {
            attrs: Default::default(),
            blocks: vec![Block::List {
                id: 0,
                list: List::Ordered(vec![ListItem(vec![
                    Block::Heading {
                        id: 1,
                        level: HeadingLevel::H2,
                        text: vec![Inline::Plain("abc".to_string())],
                    },
                    Block::List {
                        id: 2,
                        list: List::Unordered(vec![ListItem(vec![
                            Block::Paragraph {
                                id: 3,
                                text: vec![Inline::Plain("Test text".to_string())],
                            },
                            Block::List {
                                id: 4,
                                list: List::Unordered(vec![
                                    ListItem(vec![Block::Paragraph {
                                        id: 5,
                                        text: vec![Inline::Plain("abc".to_string())],
                                    }]),
                                    ListItem(vec![Block::Paragraph {
                                        id: 6,
                                        text: vec![Inline::Plain("123".to_string())],
                                    }]),
                                ]),
                            },
                        ])]),
                    },
                ])]),
            }],
        };
        render_measure_eq(doc, 9);
    }

    #[test]
    fn text_wrap_measure() {
        let paragraph = Block::Paragraph {
            id: 0,
            text: vec![Inline::Plain(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed velit sit ametx."
                    .to_string(),
            )],
        };
        render_measure_eq(paragraph, 3);
    }

    fn render_measure_eq(widget: impl DocumentWidget, height: usize) {
        let area = Rect::new(0, 0, 80, 25);
        let mut buf = Buffer::empty(area);
        let mut measure_context_a = MeasureContext::new(Default::default());
        let mut measure_context_b = MeasureContext::new(Default::default());
        let mut context = RenderContext {
            measure_context: &mut measure_context_a,
            state: &mut DocumentState::default(),
        };
        let actual_height = widget.measure(area.width, &mut context);
        context.measure_context = &mut measure_context_b;
        widget.render(area, &mut buf, &mut context);
        assert_eq!(height, measure_context_a.offset);
        assert_eq!(height, measure_context_a.offset);
        assert_eq!(height, actual_height);
    }
}
