mod block;
mod inline;
mod search;

use block::{Block, DefinitionListItem, HeadingLevel, List, ListItem};
#[allow(unused_imports)]
use html5ever::expanded_name;
use html5ever::{
    interface::{ElementFlags, NodeOrText, TreeSink},
    local_name, namespace_url, ns, parse_document,
    tendril::{StrTendril, TendrilSink},
    Attribute, LocalName, QualName,
};
use inline::{Inline, StrExt};
use log::{debug, error, trace, warn};
use ratatui::{
    prelude::{Buffer, Rect},
    style::Style,
    widgets::StatefulWidget,
};
use search::SearchState;
use std::{
    borrow::Cow,
    cmp::{self, min},
    collections::HashMap,
    io::{self, Read},
    iter::repeat,
    mem,
    rc::Rc,
    time::Instant,
};
use thiserror::Error;

const MAX_WIDTH: u16 = 120;

#[derive(Error, Debug)]
pub enum DocumentParseError {
    #[error("io error during document parsing: {0}")]
    Io(io::Error),
}

impl From<io::Error> for DocumentParseError {
    fn from(value: io::Error) -> Self {
        DocumentParseError::Io(value)
    }
}

#[derive(Clone, Default)]
pub(crate) struct MeasureContext {
    attrs: Rc<Attributes>,
    offset: usize,
    marks: Option<HashMap<String, usize>>,
    ends_on_empty: bool,
}

impl MeasureContext {
    fn new(attrs: Rc<Attributes>) -> Self {
        Self {
            attrs,
            offset: 0,
            marks: None,
            ends_on_empty: false,
        }
    }
}

impl MeasureContext {
    fn advance(&mut self, height: usize) {
        self.offset += height;
        self.ends_on_empty = false;
    }

    fn ensure_empty(&mut self) -> usize {
        if self.ends_on_empty {
            0
        } else {
            self.offset += 1;
            self.ends_on_empty = true;
            1
        }
    }
}

pub(crate) struct RenderContext<'a> {
    measure_context: &'a mut MeasureContext,
    state: &'a mut DocumentState,
}

impl<'a> RenderContext<'a> {
    pub(crate) fn with_state<'b>(&'b mut self, state: &'b mut DocumentState) -> RenderContext<'b> {
        RenderContext {
            measure_context: self.measure_context,
            state,
        }
    }

    pub(crate) fn section_height(&self, id: usize) -> usize {
        self.state.measure_cache.section_heights[id]
    }

    pub(crate) fn ends_on_empty(&self, id: usize) -> bool {
        self.state.measure_cache.ends_on_empty[id]
    }

    pub(crate) fn restore_measure_context(&mut self, id: usize) {
        self.measure_context.ends_on_empty = self.state.measure_cache.ends_on_empty[id];
    }

    pub(crate) fn update_section_cache(&mut self, id: usize, height: usize) {
        let heights = &mut self.state.measure_cache.section_heights;
        let ends_on_empty = &mut self.state.measure_cache.ends_on_empty;
        let len = heights.len();
        match len.cmp(&id) {
            cmp::Ordering::Less => {
                heights.extend(repeat(0).take(id - len));
                heights.push(height);
                ends_on_empty.extend(repeat(true).take(id - len));
                ends_on_empty.push(self.measure_context.ends_on_empty);
            }
            cmp::Ordering::Equal => {
                heights.push(height);
                ends_on_empty.push(self.measure_context.ends_on_empty);
            }
            cmp::Ordering::Greater => {
                heights[id] = height;
                ends_on_empty[id] = self.measure_context.ends_on_empty;
            }
        }
    }
}

pub(crate) trait DocumentWidget {
    fn render(&self, area: Rect, buf: &mut Buffer, context: &mut RenderContext);

    fn measure(&self, width: u16, context: &mut RenderContext) -> usize;
}

#[derive(Default, Clone, Copy)]
pub enum DocumentMode {
    #[default]
    Scroll,
    Jump,
}

#[derive(Default, Clone)]
struct MeasureCache {
    viewport: (u16, u16),
    height: usize,
    section_heights: Vec<usize>,
    ends_on_empty: Vec<bool>,
}

impl MeasureCache {
    fn invalidate(&mut self) {
        self.viewport = (0, 0);
    }
}

#[derive(Default, Clone)]
pub struct DocumentState {
    _mode: DocumentMode,
    offset: usize,
    style: Style,
    search: Option<SearchState>,
    marks: HashMap<String, usize>,
    measure_cache: MeasureCache,
    deferred_jump: Option<String>,
}

impl DocumentState {
    pub fn next(&mut self) {
        self.set_offset(self.offset.saturating_add(1));
    }

    pub fn previous(&mut self) {
        self.offset = self.offset.saturating_sub(1);
    }

    pub fn next_page(&mut self) {
        self.set_offset(
            self.offset
                .saturating_add(self.measure_cache.viewport.1 as usize * 2 / 3),
        );
    }

    pub fn previous_page(&mut self) {
        self.set_offset(
            self.offset
                .saturating_sub(self.measure_cache.viewport.1 as usize * 2 / 3),
        );
    }

    pub fn jump_to(&mut self, mark: &str) {
        if let Some(offset) = self.marks.get(mark) {
            debug!("jump to {} ({})", mark, offset);
            self.set_offset(*offset);
        } else {
            self.deferred_jump = Some(mark.to_owned());
        }
    }

    pub fn search(&mut self, query: String) {
        self.search = Some(SearchState::new(query));
        self.measure_cache.invalidate();
    }

    pub fn search_forward(&mut self) {
        if let Some(search) = &mut self.search {
            search.forward(self.offset);
        }
    }

    pub fn search_backward(&mut self) {
        if let Some(search) = &mut self.search {
            search.backward(self.offset);
        }
    }

    pub fn end_search(&mut self) {
        self.search = None;
    }

    pub(crate) fn with_offset(&self, offset: usize) -> Self {
        let mut result = self.clone();
        result.offset = offset;
        result
    }

    pub(crate) fn with_style(&self, style: Style) -> Self {
        let mut result = self.clone();
        result.style = style;
        result
    }

    fn execute_deferred_jump(&mut self) {
        if let Some(deferred) = mem::take(&mut self.deferred_jump) {
            self.jump_to(&deferred);
        }
    }

    fn set_offset(&mut self, offset: usize) {
        self.offset = min(
            offset,
            self.measure_cache
                .height
                .saturating_sub(self.measure_cache.viewport.1 as usize),
        )
    }
}

pub struct DocumentView<'a> {
    document: &'a Document,
}

impl<'a> From<&'a Document> for DocumentView<'a> {
    fn from(document: &'a Document) -> Self {
        Self { document }
    }
}

impl<'a> StatefulWidget for DocumentView<'a> {
    type State = DocumentState;

    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        let width = min(area.width, MAX_WIDTH);
        let padding = (area.width - width) / 2;
        let area = Rect::new(area.x + padding, area.y, width, area.height);
        let (vp_width, _) = state.measure_cache.viewport;
        if vp_width != width {
            let mut measurement_context = MeasureContext {
                marks: Some(Default::default()),
                ..MeasureContext::new(self.document.attrs.to_owned())
            };
            let mut context = RenderContext {
                measure_context: &mut measurement_context,
                state,
            };
            state.measure_cache.height = self.document.measure(width, &mut context);
            let MeasureContext { marks, offset, .. } = measurement_context;
            if offset != state.measure_cache.height {
                warn!(
                    "measurement error {} != {}",
                    offset, state.measure_cache.height
                );
            }
            if let Some(marks) = marks {
                state.marks = marks;
            } else {
                warn!("marks are missing");
            }
            state.execute_deferred_jump();
            if let Some(search) = &mut state.search {
                search.ensure_selected(state.offset);
            }
        }
        state.measure_cache.viewport = (width, area.height);
        let mut render_context = RenderContext {
            measure_context: &mut MeasureContext::default(),
            state,
        };
        self.document.render(area, buf, &mut render_context);
    }
}

#[derive(Debug, Default)]
struct Attributes {
    names: Vec<Vec<String>>,
}

#[derive(Debug)]
pub struct Document {
    attrs: Rc<Attributes>,
    blocks: Vec<Block>,
}

impl Document {
    pub fn parse<R: Read>(src: &mut R) -> Result<Self, DocumentParseError> {
        let start = Instant::now();
        let document = parse_document(DocumentSink::default(), Default::default())
            .from_utf8()
            .read_from(src)?;
        let delta = start.elapsed();
        debug!("document parsed in {}ms", delta.as_secs_f64() * 1000.0);
        Ok(document)
    }
}

impl DocumentWidget for Document {
    fn render(&self, area: Rect, buf: &mut Buffer, context: &mut RenderContext) {
        self.blocks[..].render(area, buf, context);
    }

    fn measure(&self, width: u16, context: &mut RenderContext<'_>) -> usize {
        self.blocks[..].measure(width, context)
    }
}

#[derive(Default, Debug)]
struct ParseContext {
    blocks: Vec<Block>,
    list_items: Vec<ListItem>,
    def_items: Vec<DefinitionListItem>,
    inline: Vec<Inline>,
    buffer: String,
    names: Vec<String>,
    in_pre: bool,
}

impl ParseContext {
    fn consume_text_buffer(&mut self) -> String {
        let out = if self.inline.is_empty() {
            self.buffer.trim_start().dedup_whitespace()
        } else {
            self.buffer.dedup_whitespace()
        };
        self.buffer.clear();
        out
    }
}

struct ParseConfig {
    skip: Vec<LocalName>,
}

struct DocumentSink {
    id: u32,
    names: Vec<Vec<String>>,
    elements: Vec<QualName>,
    stack: Vec<usize>,
    context: Vec<ParseContext>,
    skip: bool,
    config: ParseConfig,
}

impl DocumentSink {
    fn unwind(&mut self, parent: Option<&usize>) {
        while self.stack.last() != parent && self.stack.len() > 1 {
            self.pop();
        }
    }

    fn push_name(&mut self, name: String) {
        self.with_context(|c| c.names.push(name));
    }

    fn push(&mut self, parent: &usize, elem: usize) {
        self.unwind(Some(parent));
        self.stack.push(elem);
        if self.in_pre() {
            return;
        }
        if !self.skip {
            let name = &self.elements[elem];
            if self.config.skip.contains(&name.local) {
                self.skip = true;
                return;
            }
            match name.local {
                local_name!("pre") => {
                    self.end_paragraph();
                    self.with_context(|c| c.in_pre = true);
                }
                local_name!("ul") | local_name!("ol") | local_name!("dl") => {
                    self.end_paragraph();
                    self.context.push(ParseContext::default())
                }
                local_name!("li") | local_name!("dt") | local_name!("dd") => {
                    self.context.push(ParseContext::default())
                }
                local_name!("code") => self.end_inline(),
                local_name!("b") | local_name!("strong") => self.end_inline(),
                local_name!("i") | local_name!("em") => self.end_inline(),
                local_name!("br") => {
                    self.end_inline();
                    self.with_context(|c| c.inline.push(Inline::NewLine));
                }
                _ => (),
            }
        }
    }

    fn pop(&mut self) {
        if let Some(prev) = self.stack.pop() {
            if self.skip {
                if self.config.skip.contains(&self.elements[prev].local) {
                    self.skip = false;
                }
            } else if self.in_pre() {
                if let local_name!("pre") = self.elements[prev].local {
                    let id = self.next_id();
                    self.consume_names();
                    self.with_context(|c| {
                        c.blocks.push(Block::Code {
                            id,
                            lines: c
                                .buffer
                                .trim_end()
                                .lines()
                                .skip_while(|l| l.is_empty())
                                .map(|l| l.to_owned())
                                .collect(),
                        });
                        c.buffer.clear();
                        c.in_pre = false;
                    });
                }
            } else {
                match self.elements[prev].local {
                    local_name!("h1") => self.heading(HeadingLevel::H1),
                    local_name!("h2") => self.heading(HeadingLevel::H2),
                    local_name!("h3") => self.heading(HeadingLevel::H3),
                    local_name!("h4") => self.heading(HeadingLevel::H4),
                    local_name!("h5") => self.heading(HeadingLevel::H5),
                    local_name!("h6") => self.heading(HeadingLevel::H6),
                    local_name!("p") | local_name!("div") => self.end_paragraph(),
                    local_name!("ul") => {
                        if let Some(ParseContext {
                            list_items,
                            mut names,
                            ..
                        }) = self.pop_context()
                        {
                            if !list_items.is_empty() {
                                let id = self.next_id();
                                self.names.push(names);
                                self.with_context(|c| {
                                    c.blocks.push(Block::List {
                                        id,
                                        list: List::Unordered(list_items),
                                    });
                                });
                            } else {
                                self.with_context(|c| {
                                    c.names.append(&mut names);
                                });
                            }
                        }
                    }
                    local_name!("ol") => {
                        if let Some(ParseContext {
                            list_items,
                            mut names,
                            ..
                        }) = self.pop_context()
                        {
                            if !list_items.is_empty() {
                                let id = self.next_id();
                                self.names.push(names);
                                self.with_context(|c| {
                                    c.blocks.push(Block::List {
                                        id,
                                        list: List::Ordered(list_items),
                                    });
                                });
                            } else {
                                self.with_context(|c| {
                                    c.names.append(&mut names);
                                });
                            }
                        }
                    }
                    local_name!("dl") => {
                        if let Some(ParseContext {
                            def_items,
                            mut names,
                            ..
                        }) = self.pop_context()
                        {
                            if !def_items.is_empty() {
                                let id = self.next_id();
                                self.names.push(names);
                                self.with_context(|c| {
                                    c.blocks.push(Block::List {
                                        id,
                                        list: List::Definition(def_items),
                                    });
                                });
                            } else {
                                self.with_context(|c| {
                                    c.names.append(&mut names);
                                });
                            }
                        }
                    }
                    local_name!("li") => {
                        self.end_paragraph();
                        if let Some(ParseContext { blocks, .. }) = self.pop_context() {
                            if !blocks.is_empty() {
                                self.with_context(|c| {
                                    c.list_items.push(ListItem(blocks));
                                });
                            }
                        }
                    }
                    local_name!("dt") => {
                        self.end_paragraph();
                        if let Some(ParseContext { blocks, .. }) = self.pop_context() {
                            if !blocks.is_empty() {
                                self.with_context(|c| {
                                    c.def_items.push(DefinitionListItem::Term(blocks));
                                });
                            }
                        }
                    }
                    local_name!("dd") => {
                        self.end_paragraph();
                        if let Some(ParseContext { blocks, .. }) = self.pop_context() {
                            if !blocks.is_empty() {
                                self.with_context(|c| {
                                    c.def_items.push(DefinitionListItem::Details(blocks));
                                });
                            }
                        }
                    }
                    local_name!("code") => {
                        self.with_context(|c| {
                            if !c.buffer.is_empty() {
                                let text = c.consume_text_buffer();
                                c.inline.push(Inline::Code(text));
                                c.buffer.clear();
                            }
                        });
                    }
                    local_name!("b") | local_name!("strong") => {
                        self.with_context(|c| {
                            if !c.buffer.is_empty() {
                                let text = c.consume_text_buffer();
                                c.inline.push(Inline::Bold(text));
                                c.buffer.clear();
                            }
                        });
                    }
                    local_name!("i") | local_name!("em") => {
                        self.with_context(|c| {
                            if !c.buffer.is_empty() {
                                let text = c.consume_text_buffer();
                                c.inline.push(Inline::Italic(text));
                                c.buffer.clear();
                            }
                        });
                    }
                    _ => (),
                }
            }
        }
    }

    fn heading(&mut self, level: HeadingLevel) {
        self.end_last_inline();
        if self.has_inline() {
            let id = self.next_id();
            self.consume_names();
            self.with_context(|c| {
                c.blocks.push(Block::Heading {
                    id,
                    level,
                    text: mem::take(&mut c.inline),
                });
            });
        }
    }

    fn end_last_inline(&mut self) {
        self.with_context(|c| {
            let text = c.consume_text_buffer().trim_end().to_owned();
            if !text.is_empty() {
                c.inline.push(Inline::Plain(text));
                c.buffer.clear();
            }
        });
    }

    fn end_inline(&mut self) {
        self.with_context(|c| {
            let text = c.consume_text_buffer();
            if !text.is_empty() {
                c.inline.push(Inline::Plain(text));
                c.buffer.clear();
            }
        });
    }

    fn consume_names(&mut self) {
        let names = self
            .with_context(|c| mem::take(&mut c.names))
            .unwrap_or_default();
        self.names.push(names);
    }

    fn end_paragraph(&mut self) {
        self.end_last_inline();
        if self.has_inline() {
            let id = self.next_id();
            self.consume_names();
            self.with_context(|c| {
                c.blocks.push(Block::Paragraph {
                    id,
                    text: mem::take(&mut c.inline),
                });
            });
        }
    }

    fn in_pre(&mut self) -> bool {
        self.with_context(|c| c.in_pre).unwrap_or_default()
    }

    fn has_inline(&mut self) -> bool {
        self.with_context(|c| !c.inline.is_empty())
            .unwrap_or_default()
    }

    fn with_context<F, T>(&mut self, f: F) -> Option<T>
    where
        F: FnOnce(&mut ParseContext) -> T,
    {
        self.context.last_mut().map(f)
    }

    fn pop_context(&mut self) -> Option<ParseContext> {
        self.context.pop()
    }

    fn next_id(&mut self) -> u32 {
        let id = self.id;
        self.id += 1;
        id
    }
}

impl Default for DocumentSink {
    fn default() -> Self {
        Self {
            id: 0,
            names: vec![],
            elements: vec![QualName::new(None, ns!(), local_name!(""))],
            stack: vec![0],
            context: vec![ParseContext::default()],
            skip: false,
            config: ParseConfig {
                skip: vec![
                    local_name!("head"),
                    local_name!("script"),
                    local_name!("noscript"),
                    local_name!("style"),
                    // FDSS
                    local_name!("header"),
                    local_name!("nav"),
                ],
            },
        }
    }
}

impl TreeSink for DocumentSink {
    type Handle = usize;

    type Output = Document;

    fn finish(mut self) -> Self::Output {
        self.unwind(None);
        if self.context.len() > 1 {
            warn!("more than one remaining contexts: {}", self.context.len());
        }
        let blocks = self
            .pop_context()
            .map(|ParseContext { blocks, .. }| blocks)
            .unwrap_or_else(|| {
                warn!("missing render context");
                vec![]
            });
        let attrs = Rc::new(Attributes { names: self.names });
        Document { attrs, blocks }
    }

    fn parse_error(&mut self, _msg: Cow<'static, str>) {}

    fn get_document(&mut self) -> Self::Handle {
        0
    }

    fn elem_name<'a>(&'a self, target: &'a Self::Handle) -> html5ever::ExpandedName<'a> {
        self.elements[*target].expanded()
    }

    fn create_element(
        &mut self,
        name: QualName,
        attrs: Vec<Attribute>,
        _flags: ElementFlags,
    ) -> Self::Handle {
        let index = self.elements.len();
        self.elements.push(name);
        for attr in attrs {
            match attr.name.local {
                local_name!("name") | local_name!("id") => {
                    self.push_name(attr.value.to_string());
                }
                _ => (),
            }
        }
        index
    }

    fn create_comment(&mut self, _text: StrTendril) -> Self::Handle {
        0
    }

    fn create_pi(&mut self, _target: StrTendril, _data: StrTendril) -> Self::Handle {
        0
    }

    fn append(&mut self, parent: &Self::Handle, child: NodeOrText<Self::Handle>) {
        match child {
            NodeOrText::AppendNode(n) => {
                self.push(parent, n);
            }
            NodeOrText::AppendText(t) => {
                self.unwind(Some(parent));
                if self.skip {
                    trace!("skipping ({}) {}", self.skip, t);
                } else {
                    self.with_context(|c| {
                        c.buffer.push_str(&t);
                    });
                }
            }
        }
    }

    fn append_based_on_parent_node(
        &mut self,
        _element: &Self::Handle,
        _prev_element: &Self::Handle,
        _child: NodeOrText<Self::Handle>,
    ) {
    }

    fn append_doctype_to_document(
        &mut self,
        _name: StrTendril,
        _public_id: StrTendril,
        _system_id: StrTendril,
    ) {
    }

    fn get_template_contents(&mut self, _target: &Self::Handle) -> Self::Handle {
        0
    }

    fn same_node(&self, x: &Self::Handle, y: &Self::Handle) -> bool {
        x == y
    }

    fn set_quirks_mode(&mut self, _mode: html5ever::interface::QuirksMode) {}

    fn append_before_sibling(
        &mut self,
        _sibling: &Self::Handle,
        _new_node: NodeOrText<Self::Handle>,
    ) {
    }

    fn add_attrs_if_missing(&mut self, _target: &Self::Handle, _attrs: Vec<Attribute>) {}

    fn remove_from_parent(&mut self, _target: &Self::Handle) {}

    fn reparent_children(&mut self, _node: &Self::Handle, _new_parent: &Self::Handle) {}
}

#[cfg(test)]
mod test {
    use crate::{Block, Document, Inline};

    #[test]
    fn simple_paragraph() {
        let mut html = r#"
                <html>
                    <body>
                        <p>Lorem ipsum</p>
                    </body>
                </html>
            "#
        .as_bytes();
        let doc = Document::parse(&mut html).unwrap();
        assert_eq!(
            doc.blocks,
            vec![Block::Paragraph {
                id: 0,
                text: vec![Inline::Plain("Lorem ipsum".to_string())]
            }]
        )
    }

    #[test]
    fn multiline_paragraph() {
        let mut html = r#"
                <html>
                    <body>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Sed sed velit sit amet ex viverra vulputate non varius eros.
                            Nunc facilisis convallis dui vel fringilla.
                        </p>
                    </body>
                </html>
            "#
        .as_bytes();
        let doc = Document::parse(&mut html).unwrap();
        assert_eq!(
            doc.blocks,
            vec![Block::Paragraph{id:0,text:vec![Inline::Plain(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed velit sit amet ex viverra vulputate non varius eros. Nunc facilisis convallis dui vel fringilla.".to_string()
            )]}]
        )
    }

    #[test]
    fn multiple_paragraphs() {
        let mut html = r#"
                <html>
                    <body>
                        <p>Lorem ipsum</p>
                        <p>Sed sed velit</p>
                    </body>
                </html>
            "#
        .as_bytes();
        let doc = Document::parse(&mut html).unwrap();
        assert_eq!(
            doc.blocks,
            vec![
                Block::Paragraph {
                    id: 0,
                    text: vec![Inline::Plain("Lorem ipsum".to_string())]
                },
                Block::Paragraph {
                    id: 1,
                    text: vec![Inline::Plain("Sed sed velit".to_string())]
                }
            ]
        )
    }

    #[test]
    fn bold() {
        let mut html = r#"
                <html>
                    <body>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Sed sed <b>velit</b> sit amet ex viverra vulputate non varius eros.
                            Nunc facilisis <strong>convallis</strong> dui vel fringilla.
                        </p>
                    </body>
                </html>
            "#
        .as_bytes();
        let doc = Document::parse(&mut html).unwrap();
        assert_eq!(
            doc.blocks,
            vec![Block::Paragraph {
                id: 0,
                text: vec![
                    Inline::Plain(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed "
                            .to_string()
                    ),
                    Inline::Bold("velit".to_string()),
                    Inline::Plain(
                        " sit amet ex viverra vulputate non varius eros. Nunc facilisis "
                            .to_string()
                    ),
                    Inline::Bold("convallis".to_string()),
                    Inline::Plain(" dui vel fringilla.".to_string())
                ]
            }]
        )
    }

    #[test]
    fn italic() {
        let mut html = r#"
                <html>
                    <body>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Sed sed <i>velit</i> sit amet ex viverra vulputate non varius eros.
                            Nunc facilisis <em>convallis</em> dui vel fringilla.
                        </p>
                    </body>
                </html>
            "#
        .as_bytes();
        let doc = Document::parse(&mut html).unwrap();
        assert_eq!(
            doc.blocks,
            vec![Block::Paragraph {
                id: 0,
                text: vec![
                    Inline::Plain(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed "
                            .to_string()
                    ),
                    Inline::Italic("velit".to_string()),
                    Inline::Plain(
                        " sit amet ex viverra vulputate non varius eros. Nunc facilisis "
                            .to_string()
                    ),
                    Inline::Italic("convallis".to_string()),
                    Inline::Plain(" dui vel fringilla.".to_string())
                ]
            }]
        )
    }

    #[test]
    fn code() {
        let mut html = r#"
                <html>
                    <body>
                        <p><code>let a = b - c</code></p>
                    </body>
                </html>
            "#
        .as_bytes();
        let doc = Document::parse(&mut html).unwrap();
        assert_eq!(
            doc.blocks,
            vec![Block::Paragraph {
                id: 0,
                text: vec![Inline::Code("let a = b - c".to_string())]
            }]
        )
    }
}
