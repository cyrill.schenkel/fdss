# TUI Documentation Browser

Browse Dash docsets in the terminal.

```
                                                        FDSS
┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│vecnew                                                                                                  116/172109│
│──────────────────────────────────────────────────────────────────────────────────────────────────────────────────│
│ > Method       std::vec::Vec::new                                                                           Rust │
│   Method       std::vec::Vec::new_in                                                                        Rust │
│   Method       std::collections::VecDeque::new                                                              Rust │
│   Method       std::collections::VecDeque::new_in                                                           Rust │
│   Method       std::collections::vec_deque::VecDeque::new                                                   Rust │
│   Method       std::collections::vec_deque::VecDeque::new_in                                                Rust │
│   Method       visitNewClass(NewClassTree, P)                                                          Java SE20 │
│   Method       visitNewClass(NewClassTree, P)                                                          Java SE20 │
│   Method       visitNewClass(NewClassTree, P)                                                          Java SE20 │
│   Method       std::vec::Drain::take_while                                                                  Rust │
│   Method       std::vec::IntoIter::map_while                                                                Rust │
│   Method       std::vec::IntoIter::map_windows                                                              Rust │
│   Method       std::vec::IntoIter::skip_while                                                               Rust │
│   Method       std::vec::IntoIter::take_while                                                               Rust │
│   Method       std::collections::vec_deque::Drain::take_while                                               Rust │
│   Type         std::vec::IntoIter::Owned                                                                    Rust │
│   Method       std::vec::Drain::intersperse_with                                                            Rust │
│   Method       std::vec::Splice::intersperse_with                                                           Rust │
│   Method       std::collections::vec_deque::IntoIter::map_while                                             Rust │
│   Method       std::collections::vec_deque::IntoIter::map_windows                                           Rust │
│   Method       std::collections::vec_deque::IntoIter::skip_while                                            Rust │
│   Method       std::collections::vec_deque::IntoIter::take_while                                            Rust │
│   Method       std::vec::IntoIter::to_owned                                                                 Rust │
│   Method       std::vec::IntoIter::intersperse_with                                                         Rust │
│   Method       std::vec::IntoIter::borrow                                                                   Rust │
│   Method       std::vec::IntoIter::borrow_mut                                                               Rust │
│   Method       std::vec::ExtractIf::intersperse_with                                                        Rust │
│   Method       std::collections::vec_deque::Iter::intersperse_with                                          Rust │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘

```

## Keys

| Keys | Context | Action |
| --- | --- | --- |
| q | global | quit |
| 1 | global | show docset view |
| 2 | global | show download manager |
| f | docset view | open fuzzy picker |
| j | docset view | next line |
| k | docset view | previous line |
| d | docset view | next page |
| u | decset view | previous page |
| tab | fuzzy picker | select next |
| shift-tab | fuzzy picker | select previous |
| enter | fuzzy picker | open docs for selected search result |
| esc | fuzzy picker | close picker |
| j | download manager | select next |
| k | download manager | select previous |
| d | download manager | download selected docset |

## License

This Program is licensed under GPLv3.

See [LICENSE](./LICENSE)
