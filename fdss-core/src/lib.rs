pub mod find;
pub mod index;
use chrono::{DateTime, Utc};
use crossbeam::channel::Receiver;
use fdss_config::{Config, Docset};
use find::CandidateProvider;
use index::{Index, Token};
use log::{debug, error};
use rusqlite::{params, Connection, OptionalExtension, Row};
use std::{
    fmt::Display,
    path::PathBuf,
    sync::{
        atomic::{self, AtomicBool},
        Arc,
    },
    thread::{self, JoinHandle},
    time::Duration,
};
use thiserror::Error;

const DOCSET_DB_FILE: &str = "docsets.sqlite3";

#[derive(Error, Debug)]
pub enum DatabaseError {
    #[error("cannot open databse file: {0}: {1}")]
    Open(PathBuf, Box<dyn std::error::Error + Send + Sync>),
    #[error("cannot close database")]
    Close,
    #[error("cannot create database schema: {0}")]
    CreateSchema(Box<dyn std::error::Error + Send + Sync>),
    #[error("cannot insert or replace in database: {0}")]
    Put(Box<dyn std::error::Error + Send + Sync>),
    #[error("cannot query database: {0}")]
    Query(Box<dyn std::error::Error + Send + Sync>),
}

pub struct DocsetRecorder {
    shutdown: Arc<AtomicBool>,
    join_handle: JoinHandle<()>,
}

impl DocsetRecorder {
    pub fn new<E>(
        config: &Config,
        receiver: Receiver<Result<Docset, E>>,
    ) -> Result<Self, DatabaseError>
    where
        E: Display + Send + 'static,
    {
        let shutdown = Arc::new(AtomicBool::new(false));
        let shutdown_clone = shutdown.clone();
        let db = DocsetDatabase::new(config)?;
        let join_handle = thread::spawn(move || {
            let shutdown = shutdown_clone;
            while !shutdown.load(atomic::Ordering::Relaxed) {
                if let Ok(result) = receiver.recv_timeout(Duration::from_millis(200)) {
                    match result {
                        Ok(docset) => {
                            if let Err(e) = db.put(&docset) {
                                error!("docset could not be saved: {}", e);
                            }
                        }
                        Err(e) => {
                            error!("docset download failed: {}", e);
                        }
                    }
                }
            }
        });
        Ok(Self {
            shutdown,
            join_handle,
        })
    }

    pub fn shutdown(self) -> Result<(), DatabaseError> {
        self.shutdown.store(true, atomic::Ordering::Relaxed);
        self.join_handle.join().map_err(|_| DatabaseError::Close)?;
        Ok(())
    }
}

pub struct DocsetDatabase {
    conn: Connection,
}

impl DocsetDatabase {
    pub fn new(config: &Config) -> Result<Self, DatabaseError> {
        let mut db_path = config.data_dir.to_owned();
        db_path.push(DOCSET_DB_FILE);
        let conn = Connection::open(db_path.clone())
            .map_err(|e| DatabaseError::Open(db_path, Box::new(e)))?;
        conn.execute(
            r#"
                CREATE TABLE IF NOT EXISTS "docsets" (
                    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
                    "name" TEXT UNIQUE NOT NULL,
                    "version" TEXT NOT NULL,
                    "path" TEXT UNIQUE NOT NULL,
                    "last_update" TEXT NOT NULL
                )
            "#,
            [],
        )
        .map_err(|e| DatabaseError::CreateSchema(Box::new(e)))?;
        Ok(Self { conn })
    }

    pub fn put(&self, docset: &Docset) -> Result<(), DatabaseError> {
        self.conn
            .prepare(
                r#"
                    INSERT OR REPLACE INTO "docsets" (
                        "name", "version", "path", "last_update"
                    )
                    VALUES (?1, ?2, ?3, ?4)
                "#,
            )
            .and_then(|mut query| {
                query.execute(params![
                    docset.name,
                    docset.version,
                    docset.path.to_str(),
                    docset.last_update,
                ])
            })
            .map_err(|e| DatabaseError::Put(Box::new(e)))?;
        Ok(())
    }

    pub fn has(&self, name: &str) -> Result<bool, DatabaseError> {
        self.by_name(name).map(|docset| docset.is_some())
    }

    pub fn by_id(&self, id: u32) -> Result<Option<Docset>, DatabaseError> {
        self.conn
            .prepare(
                r#"
                    SELECT "id", "name", "version", "path", "last_update"
                    FROM "docsets"
                    WHERE "id" = ?1
                "#,
            )
            .and_then(|mut query| query.query_row([id], row_to_docset).optional())
            .map_err(|e| DatabaseError::Query(Box::new(e)))
    }

    pub fn by_name(&self, name: &str) -> Result<Option<Docset>, DatabaseError> {
        self.conn
            .prepare(
                r#"
                    SELECT "id", "name", "version", "path", "last_update"
                    FROM "docsets"
                    WHERE "name" = ?1
                "#,
            )
            .and_then(|mut query| query.query_row([name], row_to_docset).optional())
            .map_err(|e| DatabaseError::Query(Box::new(e)))
    }

    pub fn all(&self) -> Result<Vec<Docset>, DatabaseError> {
        self.conn
            .prepare(
                r#"
                    SELECT "id", "name", "version", "path", "last_update"
                    FROM "docsets"
                "#,
            )
            .and_then(|mut query| {
                query
                    .query_map([], row_to_docset)
                    .and_then(|docsets| docsets.collect())
            })
            .map_err(|e| DatabaseError::Query(Box::new(e)))
    }
}

fn row_to_docset(row: &Row) -> rusqlite::Result<Docset> {
    let id: u32 = row.get(0)?;
    let name: String = row.get(1)?;
    let version: String = row.get(2)?;
    let path: String = row.get(3)?;
    let last_update: DateTime<Utc> = row.get(4)?;
    Ok(Docset::new(id, name, version, path, last_update))
}

pub struct DocsetsTokenProvider {
    docsets: Vec<Docset>,
}

impl DocsetsTokenProvider {
    pub fn new(docsets: Vec<Docset>) -> Result<Self, DatabaseError> {
        Ok(Self { docsets })
    }
}

impl CandidateProvider<Token> for DocsetsTokenProvider {
    fn provide(self, injector: nucleo::Injector<Token>) {
        thread::spawn(move || {
            for docset in self.docsets.iter() {
                debug!("read docset index: {}", docset.name);
                match Index::try_from(docset) {
                    Ok(idx) => {
                        if let Err(e) = idx.for_tokens(|token| match token {
                            Ok(token) => {
                                let name = token.name.to_owned();
                                injector.push(token, |cols| {
                                    cols[0] = name.into();
                                });
                            }
                            Err(e) => {
                                error!("unable to read token: {}", e);
                            }
                        }) {
                            error!("unable to read tokens: {}", e);
                        }
                    }
                    Err(e) => {
                        error!("unable to read index: {}", e);
                    }
                }
            }
        });
    }
}
