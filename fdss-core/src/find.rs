use nucleo::{
    pattern::{CaseMatching, Normalization},
    Injector, Nucleo,
};
use std::{cmp::min, ops::Range, sync::Arc};

pub trait CandidateProvider<T> {
    fn provide(self, injector: Injector<T>);
}

pub struct Finder<T: Send + Sync + 'static> {
    nucleo: Nucleo<T>,
}

impl<T: Send + Sync + 'static> Finder<T> {
    pub fn inject_candidates<P: CandidateProvider<T>>(&self, provider: P) {
        provider.provide(self.nucleo.injector());
    }

    pub fn update_query(&mut self, query: &str) {
        self.nucleo
            .pattern
            .reparse(0, query, CaseMatching::Smart, Normalization::Smart, false);
    }

    pub fn refresh(&mut self) {
        self.nucleo.tick(10);
    }

    pub fn results(
        &self,
        range: Range<u32>,
    ) -> impl ExactSizeIterator<Item = &T> + DoubleEndedIterator {
        let snapshot = self.nucleo.snapshot();
        let count = snapshot.matched_item_count();
        let range = min(range.start, count.saturating_sub(1))..min(range.end, count);
        snapshot.matched_items(range).map(|i| i.data)
    }

    pub fn result_count(&self) -> (u32, u32) {
        let snapshot = self.nucleo.snapshot();
        (snapshot.matched_item_count(), snapshot.item_count())
    }
}

impl<T: Send + Sync + 'static> Default for Finder<T> {
    fn default() -> Self {
        let nucleo = Nucleo::new(nucleo::Config::DEFAULT, Arc::new(|| {}), None, 1);
        Self { nucleo }
    }
}
