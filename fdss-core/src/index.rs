use std::path::PathBuf;

use fdss_config::{Docset, DocsetError};
use lazy_static::lazy_static;
use regex::Regex;
use rusqlite::{Connection, OpenFlags};
use thiserror::Error;

lazy_static! {
    static ref PATH_METADATA_PREFIX: Regex = Regex::new("^(<dash_.*>)+").unwrap();
}

#[derive(Error, Debug)]
pub enum IndexError {
    #[error("docset unreadable: {0}")]
    Docset(DocsetError),
    #[error("index not found at: {0}")]
    Missing(PathBuf),
    #[error("index database error: {0}")]
    Database(rusqlite::Error),
}

impl From<DocsetError> for IndexError {
    fn from(value: DocsetError) -> Self {
        Self::Docset(value)
    }
}

impl From<rusqlite::Error> for IndexError {
    fn from(value: rusqlite::Error) -> Self {
        Self::Database(value)
    }
}

pub struct Index {
    docset_id: u32,
    conn: Connection,
    format: IndexFormat,
}

impl Index {
    pub fn for_tokens<F>(&self, mut f: F) -> Result<(), IndexError>
    where
        F: FnMut(Result<Token, IndexError>),
    {
        let statement = match self.format {
            IndexFormat::SearchIndex => r#"SELECT "name", "type", "path" FROM "searchIndex""#,
            IndexFormat::ZToken => {
                r#"
                    SELECT "i"."ZTOKENNAME" "name", "t"."ZTYPENAME" "type", "p"."ZPATH" "path"
                    FROM "ZTOKEN" "i"
                         INNER JOIN "ZTOKENTYPE" "t" ON "i"."ZTOKENTYPE" = "t"."Z_PK"
                         INNER JOIN "ZTOKENMETAINFORMATION" "m"
                             ON "i"."ZMETAINFORMATION" = "m"."Z_PK"
                         INNER JOIN "ZFILEPATH" "p" ON "m"."ZFILE" = "p"."Z_PK"
                "#
            }
        };
        let mut statement = self.conn.prepare_cached(statement)?;
        let tokens = statement.query_map([], |row| {
            let name: String = row.get(0)?;
            let kind: String = row.get(1)?;
            let path: String = row.get(2)?;
            let path = PATH_METADATA_PREFIX.replace(&path, "").into();
            Ok(Token {
                docset_id: self.docset_id,
                name,
                kind,
                path,
            })
        })?;
        for token in tokens {
            f(token.map_err(Into::into))
        }
        Ok(())
    }
}

impl TryFrom<&Docset> for Index {
    type Error = IndexError;

    fn try_from(docset: &Docset) -> Result<Self, Self::Error> {
        let index_path = docset.index_path()?;
        if !index_path.is_file() {
            return Err(IndexError::Missing(index_path));
        }
        let conn = Connection::open_with_flags(index_path, OpenFlags::SQLITE_OPEN_READ_ONLY)?;
        let format = if conn
            .prepare("SELECT 1 FROM sqlite_schema WHERE tbl_name = 'ZTOKEN' LIMIT 1")?
            .exists([])?
        {
            IndexFormat::ZToken
        } else {
            IndexFormat::SearchIndex
        };
        Ok(Self {
            docset_id: docset.id,
            conn,
            format,
        })
    }
}

#[derive(Clone, Debug)]
pub struct Token {
    pub docset_id: u32,
    pub name: String,
    pub kind: String,
    path: String,
}

impl Token {
    pub fn absolute_path(&self, docset: &Docset) -> Result<(PathBuf, Option<&str>), IndexError> {
        let mut components = self.path.splitn(2, '#');
        Ok((
            docset.documents_path()?.join(components.next().unwrap()),
            components.next(),
        ))
    }
}

enum IndexFormat {
    SearchIndex,
    ZToken,
}
