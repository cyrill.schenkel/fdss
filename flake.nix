{
  inputs = {
    naersk.url = "github:nix-community/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
      in
      rec {
        defaultPackage = naersk-lib.buildPackage {
          src = ./.;
          nativeBuildInputs = with pkgs; [ pkg-config ];
          buildInputs = with pkgs; [ openssl sqlite ];
        };
        devShell = with pkgs; mkShell {
          buildInputs =  defaultPackage.buildInputs ++ [
            cargo
            rustc
            rustfmt
            rust-analyzer
            rustPackages.clippy
            lldb
            vscode-extensions.llvm-org.lldb-vscode
          ];
          nativeBuildInputs = defaultPackage.nativeBuildInputs;
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
        };
      });
}
