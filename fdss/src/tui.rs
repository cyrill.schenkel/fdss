use crate::{document::DocumentLoader, size::Size};
use anyhow::{anyhow, Result};
use crossterm::{
    event::{self, Event, KeyCode, KeyEventKind, KeyModifiers},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use fdss_config::{Config, Docset, Feed};
use fdss_core::{find::Finder, index::Token, DocsetDatabase, DocsetRecorder, DocsetsTokenProvider};
use fdss_download::{fetch_git_feeds, read_git_feeds, BStr, DownloadManager, DownloadStatus, Url};
use fdss_html::{Document, DocumentState, DocumentView};
use log::{debug, error, trace};
use ratatui::{
    backend::CrosstermBackend,
    layout::{Alignment, Constraint, Direction, Layout},
    prelude::{Buffer, Rect},
    style::{Color, Style, Stylize},
    symbols,
    text::Text,
    widgets::{
        block::Title, Block, Borders, Clear, HighlightSpacing, LineGauge, List, ListState, Padding,
        Paragraph, StatefulWidget, Widget,
    },
    Terminal,
};
use std::{
    cmp::{max, min},
    io::{stdout, Stdout},
    iter::repeat,
    marker::PhantomData,
    ops::Deref,
    rc::Rc,
    time::Duration,
};

pub type Tui = Terminal<CrosstermBackend<Stdout>>;

pub fn init() -> Result<Tui> {
    execute!(stdout(), EnterAlternateScreen)?;
    enable_raw_mode()?;
    Ok(Terminal::new(CrosstermBackend::new(stdout()))?)
}

pub fn restore() -> Result<()> {
    execute!(stdout(), LeaveAlternateScreen)?;
    disable_raw_mode()?;
    Ok(())
}

trait EventHandler {
    type State;

    fn handle(event: &Event, state: &mut Self::State) -> bool;
}

type Cursor = Option<(u16, u16)>;

struct UiState<'a, T> {
    cursor: &'a mut Cursor,
    inner: &'a mut T,
}

impl<'a, T> UiState<'a, T> {
    fn new(cursor: &'a mut Cursor, state: &'a mut T) -> Self {
        Self {
            cursor,
            inner: state,
        }
    }

    fn descend<'b, F, U>(&'b mut self, f: F) -> UiState<'b, U>
    where
        F: Fn(&'b mut T) -> &'b mut U,
    {
        UiState {
            cursor: self.cursor,
            inner: f(self.inner),
        }
    }

    fn set_cursor(&mut self, x: u16, y: u16) {
        *self.cursor = Some((x, y));
    }
}

#[derive(Default)]
enum AppScreen {
    #[default]
    DocsetViewer,
    DocsetManager,
}

pub struct App {
    screen: AppScreen,
    docset_viewer: DocsetViewerState,
    docset_manager: DocsetManagerState,
}

impl App {
    pub fn new(config: Rc<Config>) -> Result<Self> {
        Ok(Self {
            screen: AppScreen::default(),
            docset_viewer: DocsetViewerState::new(config.to_owned()),
            docset_manager: DocsetManagerState::new(config)?,
        })
    }

    pub fn run(mut self, term: &mut Tui) -> Result<()> {
        let mut running = true;
        while running {
            term.draw(|frame| {
                let size = frame.size();
                let mut cursor = None;
                let mut state = UiState::new(&mut cursor, &mut self);
                frame.render_stateful_widget(AppView::default(), size, &mut state);
                if let Some((x, y)) = cursor {
                    frame.set_cursor(x, y);
                }
            })?;
            if event::poll(Duration::from_millis(15))? {
                let event = event::read()?;
                if !AppView::handle(&event, &mut self) {
                    handle_key_press(&event, |code, _| {
                        match code {
                            KeyCode::Char('q') => {
                                running = false;
                            }
                            KeyCode::Char('1') => {
                                self.screen = AppScreen::DocsetViewer;
                            }
                            KeyCode::Char('2') => {
                                self.screen = AppScreen::DocsetManager;
                            }
                            _ => (),
                        }
                        true
                    });
                }
            }
        }
        self.docset_manager.dm.shutdown()?;
        self.docset_manager
            .docset_recorder
            .shutdown()
            .map_err(|e| anyhow!("failed to shutdown docset recorder: {}", e))?;
        Ok(())
    }
}

#[derive(Default)]
struct AppView<'a> {
    _phantom: PhantomData<&'a ()>,
}

impl<'a> StatefulWidget for AppView<'a> {
    type State = UiState<'a, App>;

    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State)
    where
        Self: Sized,
    {
        let screen_title = match state.inner.screen {
            AppScreen::DocsetViewer => "FDSS",
            AppScreen::DocsetManager => "FDSS - DownloadManager",
        };
        let title = Title::from(screen_title.bold());
        let block = Block::default()
            .title(title.alignment(Alignment::Center))
            .padding(Padding::horizontal(1));
        let screen_area = block.inner(area);
        block.render(area, buf);
        match state.inner.screen {
            AppScreen::DocsetViewer => DocsetViewerView::default().render(
                screen_area,
                buf,
                &mut state.descend(|s| &mut s.docset_viewer),
            ),
            AppScreen::DocsetManager => DocsetManagerView::default().render(
                screen_area,
                buf,
                &mut state.descend(|s| &mut s.docset_manager),
            ),
        }
    }
}

impl<'a> EventHandler for AppView<'a> {
    type State = App;

    fn handle(event: &Event, state: &mut Self::State) -> bool {
        match state.screen {
            AppScreen::DocsetViewer => DocsetViewerView::handle(event, &mut state.docset_viewer),
            AppScreen::DocsetManager => DocsetManagerView::handle(event, &mut state.docset_manager),
        }
    }
}

enum DocsetViewerPopup {
    FuzzyFinder(FuzzyFinderState),
}

impl DocsetViewerPopup {
    fn fuzzy_finder(config: &Rc<Config>) -> Self {
        let finder = Finder::default();
        // TODO error handling
        let db = DocsetDatabase::new(config).unwrap();
        let docsets = db.all().unwrap();
        let mut docset_lut = Vec::new();
        for docset in docsets.iter() {
            let len = docset_lut.len();
            match len.cmp(&(docset.id as usize)) {
                std::cmp::Ordering::Less => {
                    docset_lut.extend(repeat(None).take(docset.id as usize - len));
                    docset_lut.push(Some(docset.to_owned()));
                }
                std::cmp::Ordering::Equal => {
                    docset_lut.push(Some(docset.to_owned()));
                }
                std::cmp::Ordering::Greater => {
                    docset_lut[docset.id as usize] = Some(docset.to_owned())
                }
            }
        }
        let provider = DocsetsTokenProvider::new(docsets).unwrap();
        finder.inject_candidates(provider);
        DocsetViewerPopup::FuzzyFinder(FuzzyFinderState {
            finder,
            docsets: docset_lut,
            ..Default::default()
        })
    }
}

struct DocsetViewerState {
    config: Rc<Config>,
    popup: Option<DocsetViewerPopup>,
    document: Option<Document>,
    doc_state: DocumentState,
}

impl DocsetViewerState {
    fn new(config: Rc<Config>) -> Self {
        let popup = Some(DocsetViewerPopup::fuzzy_finder(&config));
        Self {
            config,
            popup,
            document: None,
            doc_state: DocumentState::default(),
        }
    }
}

#[derive(Default)]
struct DocsetViewerView<'a> {
    _phantom: PhantomData<&'a ()>,
}

impl<'a> StatefulWidget for DocsetViewerView<'a> {
    type State = UiState<'a, DocsetViewerState>;

    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        if let Some(document) = state.inner.document.as_ref() {
            Into::<DocumentView>::into(document).render(area, buf, &mut state.inner.doc_state);
        }

        // render popups
        if let Some(popup) = state.inner.popup.as_mut() {
            let popup_area = popup_area(&area);
            Clear.render(popup_area, buf);
            let block = Block::new().borders(Borders::all());
            let popup_inner = block.inner(popup_area);
            block.render(popup_area, buf);
            let DocsetViewerPopup::FuzzyFinder(popup_state) = popup;
            FuzzyFinderPopup::default().render(
                popup_inner,
                buf,
                &mut UiState {
                    cursor: state.cursor,
                    inner: popup_state,
                },
            );
        }
    }
}

impl<'a> EventHandler for DocsetViewerView<'a> {
    type State = DocsetViewerState;

    fn handle(event: &Event, state: &mut Self::State) -> bool {
        if let Some(popup) = state.popup.as_mut() {
            let DocsetViewerPopup::FuzzyFinder(popup_state) = popup;
            if FuzzyFinderPopup::handle(event, popup_state) {
                true
            } else {
                let token = popup_state.token.to_owned();
                handle_key_press(event, |code, _| match code {
                    KeyCode::Esc => {
                        state.popup = None;
                        true
                    }
                    KeyCode::Enter => {
                        debug!("select {:?}", token);
                        state.popup = None;
                        if let Some(token) = token {
                            match DocumentLoader::new(&state.config)
                                .and_then(|loader| loader.load(token))
                            {
                                Ok((document, doc_state)) => {
                                    state.doc_state = doc_state;
                                    state.document = Some(document);
                                }
                                Err(e) => error!("unable to load document for: {}", e),
                            }
                        }
                        trace!("{:?}", state.document);
                        true
                    }
                    _ => false,
                })
            }
        } else {
            handle_key_press(event, |code, _| match code {
                KeyCode::Char('f') => {
                    if state.popup.is_none() {
                        state.popup = Some(DocsetViewerPopup::fuzzy_finder(&state.config));
                        true
                    } else {
                        false
                    }
                }
                KeyCode::Char('j') => {
                    state.doc_state.next();
                    true
                }
                KeyCode::Char('k') => {
                    state.doc_state.previous();
                    true
                }
                KeyCode::Char(' ') | KeyCode::Char('d') | KeyCode::PageDown => {
                    state.doc_state.next_page();
                    true
                }
                KeyCode::Char('u') | KeyCode::PageUp => {
                    state.doc_state.previous_page();
                    true
                }
                _ => false,
            })
        }
    }
}

// TODO make fuzzy finder reusable for arbitrary `T`s in place of `Token`
#[derive(Default)]
struct FuzzyFinderState {
    finder: Finder<Token>,
    query: LineInputState,
    selected: u32,
    count: (u32, u32),
    docsets: Vec<Option<Docset>>,
    token: Option<Token>,
}

#[derive(Default)]
struct FuzzyFinderPopup<'a> {
    _phantom: PhantomData<&'a ()>,
}

impl<'a> StatefulWidget for FuzzyFinderPopup<'a> {
    type State = UiState<'a, FuzzyFinderState>;

    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        state.inner.finder.refresh();
        state.inner.count = state.inner.finder.result_count();
        let counter = format!("{}/{}", state.inner.count.0, state.inner.count.1);
        let layout = Layout::new(
            Direction::Vertical,
            [Constraint::Length(2), Constraint::Fill(1)],
        );
        let query_block = Block::new()
            .borders(Borders::BOTTOM)
            .border_style(Style::new().fg(Color::DarkGray));
        let rows = layout.split(area);
        let query_area = query_block.inner(rows[0]);
        let query_cols = Layout::new(
            Direction::Horizontal,
            [
                Constraint::Fill(1),
                Constraint::Length(counter.len() as u16),
            ],
        )
        .split(query_area);
        LineInputView::default().render(query_cols[0], buf, &mut state.descend(|s| &mut s.query));
        Paragraph::new(counter).render(query_cols[1], buf);
        query_block.render(rows[0], buf);
        let selected = state.inner.selected % rows[1].height as u32;
        let offset = state.inner.selected - selected;
        let items = state
            .inner
            .finder
            .results(offset..(offset + rows[1].height as u32))
            .collect::<Vec<&Token>>();
        let highlight_symbol = " > ";
        let item_width = rows[1].width as usize - highlight_symbol.len();
        let list = items
            .iter()
            .map(|t| {
                let docset = &state.inner.docsets[t.docset_id as usize];
                format_token(item_width, t, docset.as_ref())
            })
            .collect::<List>();
        if selected as usize >= list.len() {
            state.inner.selected = 0;
            state.inner.token = items.first().map(|t| (**t).to_owned());
        } else {
            state.inner.token = items.get(selected as usize).map(|t| (**t).to_owned());
        }
        let mut list_state = ListState::default().with_selected(Some(selected as usize));
        StatefulWidget::render(
            list.highlight_symbol(" > ")
                .highlight_spacing(HighlightSpacing::Always)
                .highlight_style(Style::default().bold()),
            rows[1],
            buf,
            &mut list_state,
        )
    }
}

fn format_token(width: usize, token: &Token, docset: Option<&Docset>) -> String {
    let docset = if let Some(docset) = docset {
        &docset.name
    } else {
        ""
    };
    let width = width - 15;
    let token_width = (width - 12).saturating_sub(docset.len()) + 12;
    let docset_width = width.saturating_sub(token_width);
    format!(
        "{:<12} {:<token_width$} {}",
        token.kind,
        token.name,
        &docset[0..docset_width]
    )
}

impl<'a> EventHandler for FuzzyFinderPopup<'a> {
    type State = FuzzyFinderState;

    fn handle(event: &Event, state: &mut Self::State) -> bool {
        if LineInputView::handle(event, &mut state.query) {
            state.finder.update_query(&state.query.text);
            true
        } else {
            handle_key_press(event, |code, _| match code {
                KeyCode::BackTab => {
                    if state.selected == 0 {
                        state.selected = state.count.0 - 1;
                    } else {
                        state.selected -= 1;
                    }
                    true
                }
                KeyCode::Tab => {
                    state.selected = state.selected.saturating_add(1);
                    true
                }
                _ => false,
            })
        }
    }
}

#[derive(Default)]
enum DocsetManagerScreen {
    #[default]
    Feeds,
}

struct DocsetManagerState {
    screen: DocsetManagerScreen,
    feeds: FeedSelectionState,
    dm: DownloadManager,
    docset_recorder: DocsetRecorder,
}

impl DocsetManagerState {
    fn new(config: Rc<Config>) -> Result<Self> {
        let dm = DownloadManager::new(config.deref().to_owned());
        let docset_recorder = DocsetRecorder::new(&config, dm.finished.clone())?;
        Ok(Self {
            screen: DocsetManagerScreen::default(),
            feeds: FeedSelectionState::new(config),
            dm,
            docset_recorder,
        })
    }
}

#[derive(Default)]
struct DocsetManagerView<'a> {
    _phantom: PhantomData<&'a ()>,
}

impl<'a> StatefulWidget for DocsetManagerView<'a> {
    type State = UiState<'a, DocsetManagerState>;

    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State)
    where
        Self: Sized,
    {
        let downloads = state.inner.dm.downloads();
        let rows = if downloads.is_empty() {
            Layout::new(Direction::Vertical, [Constraint::Fill(1)])
        } else {
            Layout::new(
                Direction::Vertical,
                [
                    Constraint::Length(downloads.len() as u16),
                    Constraint::Fill(1),
                ],
            )
        }
        .split(area);
        if !downloads.is_empty() {
            DownloadManagerView { downloads }.render(rows[0], buf);
            FeedSelectionView::default().render(rows[1], buf, &mut state.descend(|s| &mut s.feeds));
        } else {
            FeedSelectionView::default().render(rows[0], buf, &mut state.descend(|s| &mut s.feeds));
        }
    }
}

impl<'a> EventHandler for DocsetManagerView<'a> {
    type State = DocsetManagerState;

    fn handle(event: &Event, state: &mut Self::State) -> bool {
        match state.screen {
            DocsetManagerScreen::Feeds => FeedSelectionView::handle(event, state),
        }
    }
}

struct DownloadManagerView {
    downloads: Vec<DownloadStatus>,
}

impl Widget for DownloadManagerView {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let row_layout = Layout::new(
            Direction::Horizontal,
            [
                Constraint::Length(16),
                Constraint::Fill(1),
                Constraint::Length(12),
            ],
        )
        .spacing(1);
        let mut rows = area.rows();
        for download in self.downloads {
            if let Some(feed) = download.feed {
                if download.done {
                    continue;
                }
                let name = Text::from(if feed.name.len() < 16 {
                    feed.name
                } else {
                    format!("{}…", &feed.name[0..14])
                });
                let speed = Text::from(format!("{}/s", Size::from(download.rate)))
                    .alignment(Alignment::Right);
                if let Some(row) = rows.next() {
                    let cells = row_layout.split(row);
                    name.render(cells[0], buf);
                    let progress = if download.size > 0 {
                        download.transferred as f64 / download.size as f64
                    } else {
                        0.0
                    };
                    let label = format!("{:>3}%", (progress * 100.0).round() as u8);
                    LineGauge::default()
                        .ratio(progress)
                        .label(label)
                        .gauge_style(Style::new().fg(Color::White).bg(Color::DarkGray))
                        .line_set(symbols::line::THICK)
                        .render(cells[1], buf);
                    speed.render(cells[2], buf);
                } else {
                    break; // no room to display more downloads
                }
            }
        }
    }
}

struct FeedSelectionState {
    config: Rc<Config>,
    feeds: Vec<Feed>,
    list: ListState,
}

impl FeedSelectionState {
    fn new(config: Rc<Config>) -> Self {
        Self {
            config,
            feeds: Vec::default(),
            list: ListState::default(),
        }
    }

    fn read(&mut self) -> Result<()> {
        let db = DocsetDatabase::new(&self.config)
            .map_err(|e| anyhow!("cannot open docset database: {}", e))?;
        // TODO put url somewhere else
        let url: Url = Url::from_bytes(BStr::new(b"git@github.com:Kapeli/feeds.git"))?;
        self.feeds.clear();
        for feed in read_git_feeds(&url)? {
            let feed = feed?;
            if !db.has(&feed.name)? {
                self.feeds.push(feed)
            }
        }
        self.feeds.sort_unstable_by(|a, b| a.name.cmp(&b.name));
        Ok(())
    }

    fn update(&mut self) -> Result<()> {
        // TODO put url somewhere else
        let url: Url = Url::from_bytes(BStr::new(b"git@github.com:Kapeli/feeds.git"))?;
        self.feeds.clear();
        fetch_git_feeds(&url)?;
        self.read()?;
        Ok(())
    }
}

#[derive(Default)]
struct FeedSelectionView<'a> {
    _phantom: PhantomData<&'a ()>,
}

impl<'a> StatefulWidget for FeedSelectionView<'a> {
    type State = UiState<'a, FeedSelectionState>;

    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        if state.inner.feeds.is_empty() {
            if let Err(e) = state.inner.read() {
                error!("feeds cannot be read: {}", e);
            }
        }
        match state.inner.list.selected() {
            Some(i) => {
                if state.inner.feeds.is_empty() {
                    state.inner.list.select(None)
                } else {
                    state
                        .inner
                        .list
                        .select(Some(min(i, state.inner.feeds.len() - 1)))
                }
            }
            None => {
                if !state.inner.feeds.is_empty() {
                    state.inner.list.select(Some(0));
                }
            }
        }
        StatefulWidget::render(
            state
                .inner
                .feeds
                .iter()
                .map(|feed| feed.name.to_string())
                .collect::<List>()
                .highlight_symbol("-> ")
                .highlight_spacing(HighlightSpacing::Always)
                .highlight_style(Style::default().bold()),
            area,
            buf,
            &mut state.inner.list,
        );
    }
}

impl<'a> EventHandler for FeedSelectionView<'a> {
    type State = DocsetManagerState;

    fn handle(event: &Event, state: &mut Self::State) -> bool {
        let dm = &state.dm;
        let state = &mut state.feeds;
        handle_key_press(event, |code, _| match code {
            KeyCode::Char('j') => {
                let i = match state.list.selected() {
                    Some(i) => min(i.saturating_add(1), state.feeds.len() - 1),
                    None => state.list.offset(),
                };
                state.list.select(Some(i));
                true
            }
            KeyCode::Char('k') => {
                let i = match state.list.selected() {
                    Some(i) => i.saturating_sub(1),
                    None => state.list.offset(),
                };
                state.list.select(Some(i));
                true
            }
            KeyCode::Char('u') => {
                if let Err(e) = state.update() {
                    error!("feeds could not be updated: {}", e);
                }
                true
            }
            KeyCode::Char('d') => {
                if let Some(i) = state.list.selected() {
                    let feed = state.feeds.get(i).map(ToOwned::to_owned);
                    if let Some(feed) = feed {
                        state.feeds.remove(i);
                        if let Err(e) = dm.download(feed.clone()) {
                            error!("cannot download '{}': {}", feed.name, e);
                        }
                    }
                }
                true
            }
            _ => false,
        })
    }
}

#[derive(Default)]
struct LineInputState {
    cursor: u16,
    text: String,
}

#[derive(Default)]
struct LineInputView<'a> {
    _phantom: PhantomData<&'a ()>,
}

impl<'a> StatefulWidget for LineInputView<'a> {
    type State = UiState<'a, LineInputState>;

    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        let text = Text::from(state.inner.text.to_owned());
        text.render(area, buf);
        state.set_cursor(area.x + state.inner.cursor, area.y);
    }
}

impl<'a> EventHandler for LineInputView<'a> {
    type State = LineInputState;

    fn handle(event: &Event, state: &mut Self::State) -> bool {
        handle_key_press(event, |code, _| match code {
            KeyCode::Char(c) => {
                state.text.push(*c);
                state.cursor += 1;
                true
            }
            KeyCode::Backspace => {
                if state.text.pop().is_some() {
                    state.cursor -= 1;
                }
                true
            }
            _ => false,
        })
    }
}

fn handle_key_press<F>(event: &Event, f: F) -> bool
where
    F: FnOnce(&KeyCode, &KeyModifiers) -> bool,
{
    if let Event::Key(e) = event {
        if e.kind == KeyEventKind::Press {
            f(&e.code, &e.modifiers)
        } else {
            false
        }
    } else {
        false
    }
}

const TOP_MAX: u16 = 4;
const BOTTOM_MAX: u16 = 4;
const TOP_MIN: u16 = 1;
const BOTTOM_MIN: u16 = 3;
const HORIZONTAL_MIN: u16 = 2;
const HORIZONTAL_PERCENT: u16 = 5;
const VERTICAL_PERCENT: u16 = 5;

fn popup_area(rect: &Rect) -> Rect {
    let top = max(TOP_MIN, min(TOP_MAX, rect.height * VERTICAL_PERCENT / 100));
    let bottom = max(
        BOTTOM_MIN,
        min(BOTTOM_MAX, rect.height * VERTICAL_PERCENT / 100),
    );
    let horizontal = max(HORIZONTAL_MIN, rect.width * HORIZONTAL_PERCENT / 100);
    Rect {
        x: horizontal + 1,
        y: top + 1,
        width: rect.width - horizontal * 2,
        height: rect.height - top - bottom,
    }
}
