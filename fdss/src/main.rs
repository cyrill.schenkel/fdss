mod document;
mod logger;
mod size;
mod tui;

use anyhow::Result;
use fdss_config::Config;
use log::error;
use logger::Logger;
use std::backtrace::Backtrace;
use std::panic;
use std::rc::Rc;
use tui::App;

fn main() -> Result<()> {
    let config = Config::new()?;

    Logger::init(&config)?;

    let mut term = tui::init()?;

    panic::set_hook(Box::new(|panic| {
        tui::restore().unwrap();
        let backtrace = Backtrace::capture().to_string();
        error!("Panic\n{}\n\nBacktrace:\n{}", panic, backtrace);
        eprintln!("{}\n\nBacktrace:\n{}", panic, backtrace);
    }));

    let result = App::new(Rc::new(config))?.run(&mut term);

    tui::restore()?;

    result
}
