use anyhow::{anyhow, Result};
use chrono::Local;
use fdss_config::Config;
use log::{max_level, set_boxed_logger, set_max_level, LevelFilter, Log, Metadata, Record};
use std::{
    cmp::min,
    fs::{create_dir_all, File, OpenOptions},
    io::Write,
    sync::Mutex,
};

pub struct Logger {
    file: Mutex<File>,
}

impl Logger {
    pub fn init(config: &Config) -> Result<()> {
        let mut log_file = config.data_dir.to_owned();
        log_file.push("log");
        if !log_file.exists() {
            create_dir_all(log_file.parent().unwrap())?;
            File::create(log_file.clone())?;
        }
        let file = OpenOptions::new().append(true).open(log_file)?;
        let logger = Box::new(Logger {
            file: Mutex::new(file),
        });
        set_boxed_logger(logger).map_err(|e| anyhow!("failed to set logger: {}", e))?;
        set_max_level(LevelFilter::Debug);
        Ok(())
    }
}

impl Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        if metadata.target().starts_with("html5ever::") {
            min(LevelFilter::Warn, max_level()) >= metadata.level()
        } else {
            max_level() >= metadata.level()
        }
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let mut file = self.file.lock().unwrap();
            writeln!(
                file,
                "{}\t{}\t{} {}",
                Local::now(),
                record.level(),
                record.target(),
                record.args()
            )
            .unwrap()
        }
    }

    fn flush(&self) {
        todo!()
    }
}
