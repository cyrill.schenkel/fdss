use anyhow::{anyhow, Result};
use fdss_config::Config;
use fdss_core::{index::Token, DocsetDatabase};
use fdss_html::{Document, DocumentState};
use std::fs::File;

pub struct DocumentLoader {
    db: DocsetDatabase,
}

impl DocumentLoader {
    pub fn new(config: &Config) -> Result<Self> {
        Ok(Self {
            db: DocsetDatabase::new(config)?,
        })
    }

    pub fn load(&self, token: Token) -> Result<(Document, DocumentState)> {
        let docset = self.db.by_id(token.docset_id)?;
        let (path, fragment) =
            token.absolute_path(&docset.ok_or(anyhow!("could not find docset"))?)?;
        let mut file = File::open(path)?;
        let mut state = DocumentState::default();
        if let Some(fragment) = fragment {
            state.jump_to(fragment);
        } else {
            state.jump_to(&token.name);
        }
        Ok((Document::parse(&mut file)?, state))
    }
}
