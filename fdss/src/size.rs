use std::fmt::Display;

pub enum Size {
    Bytes(u16),
    Kibibytes(u16),
    Mibibytes(u16),
    Gibibytes(u16),
    Tebibytes(u16),
    Pebibytes(u16),
}

impl Display for Size {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Size::Bytes(c) => write!(f, "{}B", c),
            Size::Kibibytes(c) => write!(f, "{}KiB", c),
            Size::Mibibytes(c) => write!(f, "{}MiB", c),
            Size::Gibibytes(c) => write!(f, "{}GiB", c),
            Size::Tebibytes(c) => write!(f, "{}TiB", c),
            Size::Pebibytes(c) => write!(f, "{}PiB", c),
        }
    }
}

impl From<u64> for Size {
    fn from(mut value: u64) -> Self {
        let mut magnitude = 0;
        while value > 1024 {
            value >>= 10;
            magnitude += 1;
        }
        match magnitude {
            0 => Self::Bytes(value as u16),
            1 => Self::Kibibytes(value as u16),
            2 => Self::Mibibytes(value as u16),
            3 => Self::Gibibytes(value as u16),
            4 => Self::Tebibytes(value as u16),
            _ => Self::Pebibytes(value as u16),
        }
    }
}
