use chrono::DateTime;
pub use chrono::Utc;
use std::{collections::VecDeque, fs::read_dir, io, path::PathBuf};
use thiserror::Error;

const APP_NAME: &str = "fdss";
const DOCSET_DIR: &str = "docsets";
const CONTENTS_DIR: &str = "Contents";
const INDEX_FILE_PATH: &str = "Resources/docSet.dsidx";
const DOCUMENTS_PATH: &str = "Resources/Documents";

#[derive(Error, Debug)]
pub enum ConfigError {
    #[error("cache dir cannot be determined")]
    MissingCacheDir,
    #[error("data dir cannot be determined")]
    MissingDataDir,
}

#[derive(Clone, Debug)]
pub struct Config {
    pub cache_dir: PathBuf,
    pub data_dir: PathBuf,
}

impl Config {
    pub fn new() -> Result<Self, ConfigError> {
        let mut cache_dir = dirs::cache_dir().ok_or(ConfigError::MissingCacheDir)?;
        cache_dir.push(APP_NAME);

        let mut data_dir = dirs::data_dir().ok_or(ConfigError::MissingDataDir)?;
        data_dir.push(APP_NAME);

        Ok(Self {
            cache_dir,
            data_dir,
        })
    }

    pub fn docset_dir(&self) -> PathBuf {
        let mut docset_dir = self.data_dir.to_owned();
        docset_dir.push(DOCSET_DIR);
        docset_dir
    }
}

#[derive(Error, Debug)]
pub enum DocsetError {
    #[error("cannot find `Contents` dir in {0}")]
    ContentsNotFound(PathBuf),
    #[error("{0}")]
    IoError(io::Error),
}

impl From<io::Error> for DocsetError {
    fn from(value: io::Error) -> Self {
        Self::IoError(value)
    }
}

#[derive(Clone, Debug)]
pub struct Docset {
    pub id: u32,
    pub name: String,
    pub version: String,
    pub path: PathBuf,
    pub last_update: DateTime<Utc>,
}

impl Docset {
    pub fn new(
        id: u32,
        name: impl Into<String>,
        version: impl Into<String>,
        path: impl Into<PathBuf>,
        last_update: impl Into<DateTime<Utc>>,
    ) -> Self {
        Self {
            id,
            name: name.into(),
            version: version.into(),
            path: path.into(),
            last_update: last_update.into(),
        }
    }

    pub fn from_feed(feed: Feed, config: &Config) -> Self {
        let path = feed.install_path(config);
        Self::new(0, feed.name, feed.version, path, Utc::now())
    }

    pub fn find_contents(&self) -> Result<PathBuf, DocsetError> {
        let mut dir_queue = VecDeque::<PathBuf>::new();
        dir_queue.push_back(self.path.to_owned());
        while let Some(dir) = dir_queue.pop_front() {
            for entry in read_dir(dir)? {
                let entry = entry?;
                let entry_type = entry.file_type()?;
                if entry_type.is_dir() {
                    if entry.file_name() == CONTENTS_DIR {
                        return Ok(entry.path());
                    } else {
                        dir_queue.push_back(entry.path());
                    }
                }
            }
        }
        Err(DocsetError::ContentsNotFound(self.path.to_owned()))
    }

    pub fn index_path(&self) -> Result<PathBuf, DocsetError> {
        self.find_contents()
            .map(|contents| contents.join(INDEX_FILE_PATH))
    }

    pub fn documents_path(&self) -> Result<PathBuf, DocsetError> {
        self.find_contents()
            .map(|contents| contents.join(DOCUMENTS_PATH))
    }
}

#[derive(Eq, PartialEq, PartialOrd, Ord, Clone, Debug)]
pub struct Feed {
    pub name: String,
    pub version: String,
    pub urls: Vec<String>,
}

impl Feed {
    pub fn new(
        name: impl Into<String>,
        version: impl Into<String>,
        urls: impl Into<Vec<String>>,
    ) -> Self {
        Self {
            name: name.into(),
            version: version.into(),
            urls: urls.into(),
        }
    }

    pub fn install_path(&self, config: &Config) -> PathBuf {
        let mut path = config.docset_dir();
        path.push(self.name.replace(' ', "_"));
        path
    }
}
